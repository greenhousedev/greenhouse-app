import {Alert} from 'react-native';

export default class AlertUtils {
  static yesNo(
    title: string,
    message: string,
    yesCallback?: () => any,
    noCallback?: () => any,
  ) {
    Alert.alert(title, message, [
      {
        text: 'Yes',
        onPress: yesCallback,
        style: 'destructive',
      },
      {
        text: 'No',
        style: 'cancel',
        onPress: noCallback,
      },
    ]);
  }
}

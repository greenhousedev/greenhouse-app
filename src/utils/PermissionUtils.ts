import {PermissionsAndroid, Platform} from 'react-native';
import {Alert} from 'react-native';
import Permissions, {PERMISSIONS} from 'react-native-permissions';

export class PermissionUtils {
  // User has authorized this permission
  static STATUS_AUTHORIZED = 'authorized';

  // User has denied this permission at least once. On iOS this means that the user will not be prompted again.
  // Android users can be prompted multiple times until they select 'Never ask me again'
  static STATUS_DENIED = 'denied';

  // iOS - this means user is not able to grant this permission,
  // either because it's not supported by the device or because it has been blocked by parental controls.
  // Android - this means that the user has selected 'Never ask me again' while denying permission
  static STATUS_RESTRICTED = 'restricted';

  // User has not yet been prompted with a permission dialog
  static STATUS_UNDETERMINED = 'undetermined';

  static checkWithAlertPhotos(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return PermissionUtils.checkWithAlertForPermission(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.PHOTO_LIBRARY
        : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
      Platform.OS === 'ios'
        ? 'Can we access your photos?'
        : 'Can we access your storage?',
      'We need access so you can set your profile pic',
    );
  }

  static checkWithAlertCamera(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return PermissionUtils.checkWithAlertForPermission(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.CAMERA
        : PERMISSIONS.ANDROID.CAMERA,
      'Can we access your camera?',
      'We need access so you can set a photo for your template',
    );
  }

  static checkWithAlertLocation(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return PermissionUtils.checkWithAlertForPermission(
      'location',
      'Location Permission Access',
      'We need to access your location to show destinations near you',
    );
  }

  static checkLocation(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return Permissions.check(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    );
  }

  static checkCamera(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return this.checkPermission(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.CAMERA
        : PERMISSIONS.ANDROID.CAMERA,
    );
  }

  static checkPhotos(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return this.checkPermission(
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.PHOTO_LIBRARY
        : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
    );
  }

  static checkDownloads(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return this.checkPermission(
      Platform.OS === 'ios' ? '' : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
    );
  }

  static checkWithAlertDownloads(): Promise<
    'unavailable' | 'denied' | 'blocked' | 'granted'
  > {
    return PermissionUtils.checkWithAlertForPermission(
      Platform.OS === 'ios' ? '' : PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      'Can we access your downloads folder?',
      'We need access so you can download the attachments',
    );
  }

  static checkPermission(
    permission: string,
  ): Promise<'unavailable' | 'denied' | 'blocked' | 'granted'> {
    return Permissions.check(permission);
  }

  static checkWithAlertForPermission(
    permission: string,
    title: string,
    message: string,
    cancelText: string = 'No',
    yesText: string = 'Yes',
    settingsText: string = 'Open Settings',
  ): Promise<'unavailable' | 'denied' | 'blocked' | 'granted'> {
    console.log(
      'PermissionUtils::checkWithAlertForPermission permission=' + permission,
    );
    return new Promise((resolve, reject) => {
      console.log(
        'PermissionUtils::checkWithAlertForPermission permission=' +
          permission +
          ', in promise',
      );
      Permissions.check(permission)
        .then((response) => {
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          console.log(
            'PermissionUtils::checkWithAlertForPermission permission=' +
              permission +
              ', status=' +
              response,
          );
          if (response === 'granted') {
            resolve(response);
            return;
          }

          Alert.alert(title, message, [
            {
              text: cancelText,
              onPress: () => {
                console.log('Permission denied');
                reject('Permission denied');
              },
              style: 'cancel',
            },
            response !== 'blocked'
              ? {
                  text: yesText,
                  onPress: () => {
                    Permissions.request(permission)
                      .then((response) => {
                        // Returns once the user has chosen to 'allow' or to 'not allow' access
                        // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
                        console.log(
                          'PermissionUtils::checkWithAlertForPermission permission=' +
                            permission +
                            ', onYes status=' +
                            response,
                        );
                        resolve(response);
                      })
                      .catch((reason) => {
                        console.log(
                          'PermissionUtils::checkWithAlertForPermission permission=' +
                            permission +
                            ', onYes reason=' +
                            reason,
                        );
                        reject(reason);
                      });
                  },
                }
              : {
                  text: settingsText,
                  onPress: () => {
                    if (Platform.OS === 'android') {
                      //TODO add support to open settings
                      return;
                    }
                    Permissions.openSettings();
                  },
                },
          ]);
        })
        .catch((reason) => {
          console.log(
            'PermissionUtils::checkWithAlertForPermission permission=' +
              permission +
              ', reason=' +
              reason,
          );
          reject(reason);
        });
    });
  }

  _requestPermission(
    permission: String,
  ): Promise<'unavailable' | 'denied' | 'blocked' | 'granted'> {
    return new Promise<'unavailable' | 'denied' | 'blocked' | 'granted'>(
      (resolve, reject) => {
        Permissions.request(permission)
          .then((response) => {
            // Returns once the user has chosen to 'allow' or to 'not allow' access
            // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
            resolve(response);
          })
          .catch((reason) => {
            reject(reason);
          });
      },
    );
  }
}

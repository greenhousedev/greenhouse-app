import FirebaseData from "./FirebaseData";

export default class AppConfig extends FirebaseData {

    static DEFAULT_TIME =  15 * 1000;

    debugUser: string;
    photoRequestTimeout: number = AppConfig.DEFAULT_TIME;

    constructor(data: any = null) {
        super(data);

        this.photoRequestTimeout = (data && data.photoRequestTimeout) ?? AppConfig.DEFAULT_TIME;
    }
}

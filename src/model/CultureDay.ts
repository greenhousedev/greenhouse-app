import FirebaseData from './FirebaseData';
import CultureParameter from './CultureParameter';
import _ from 'lodash';

export class CultureDay extends FirebaseData {
  id: string = '';
  dayIndex: number = 0;
  parametersConfigurationEntries: CultureParameter[] = [];

  constructor(data?: any) {
    super(data);

    Object.assign(this, data);

    if (data) {
      this.parametersConfigurationEntries = data.parametersConfigurationEntries
        ? _.toArray(data.parametersConfigurationEntries).map(
            (value) => new CultureParameter(value),
          )
        : [];
    }
  }

  sortParams() {
    this.parametersConfigurationEntries.sort((a, b) => a.compare(b));
  }

  compare(day: CultureDay): number {
    if (this.dayIndex < day.dayIndex) {
      return -1;
    }
    if (this.dayIndex > day.dayIndex) {
      return 1;
    }
    return 0;
  }

  static sortDays(days: CultureDay[]): CultureDay[] {
    if (!days) {
      return [];
    }
    days.sort((a, b) => a.compare(b));
    days.forEach((value) => value.sortParams());
    return days;
  }
}

import FirebaseData from './FirebaseData';

export default class CultureTime extends FirebaseData {
  static createNew(hour: number = 0, minutes: number = 0): CultureTime {
    const t = new CultureTime();
    t._hour = hour;
    t._minutes = minutes;
    return t;
  }

  _hour: number = 0;
  _minutes: number = 0;

  constructor(data?: any = null) {
    super(data);

    if (data) {
      this._hour = data._hour ? parseInt(data._hour) : 0;
      this._minutes = data._minutes ? parseInt(data._minutes) : 0;
    }
  }

  toPrettyText() {
    return `${(this._hour < 10 ? '0' : '') + this._hour.toString()}:${
      (this._minutes < 10 ? '0' : '') + this._minutes.toString()
    }`;
  }

  compare(time: CultureTime): number {
    if (this._hour < time._hour) {
      return -1;
    }
    if (this._hour > time._hour) {
      return 1;
    }
    if (this._minutes < time._minutes) {
      return -1;
    }
    if (this._minutes > time._minutes) {
      return 1;
    }
    return 0;
  }
}

import UserPot from './UserPot';
import _ from 'lodash';
import FirebaseData from './FirebaseData';
import CultureTemplate from './CultureTemplate';

export default class User extends FirebaseData {
  static initUser(email: string): User {
    const u = new User();
    u.email = email;
    return u;
  }

  email: string | undefined;
  mainPotId: string | undefined;
  pots: UserPot[] = [];
  templates: CultureTemplate[] = [];
  fcmTokens: String[] = [];

  sentConfirmationEmail: boolean = false;
  confirmed: boolean = false;

  constructor(data: any = null) {
    super(data);
    Object.assign(this, data);

    if (data) {
      this.pots = data.pots
        ? _.toArray(data.pots).map((value) => new UserPot(value))
        : [];
      this.templates = data.templates
        ? _.toArray(data.templates).map((value) => new CultureTemplate(value))
        : [];
      this.fcmTokens = data.fcmTokens ?? [];
    }
  }

  get mainPot(): UserPot | undefined {
    let pot = undefined;
    if (this.mainPotId) {
      pot = _.find(this.pots, (value) => value.id === this.mainPotId);
    }
    return pot;
  }
}

import FirebaseData from './FirebaseData';
import CultureSchedule from './CultureSchedule';
import PhotoRequest from './PhotoRequest';

export class RelayState extends FirebaseData {
  // values 0 or 1
  fan: number = 0;
  light: number = 0;
  pump: number = 0;

  constructor(data: any = null) {
    super(data);
    console.log("UserPot::data "+JSON.stringify(data));
    Object.assign(this, data);
  }
}

export class Sensors extends FirebaseData {
  humidity: number = 0;
  temperature: number = 0;
  ph: number = 0;
  light: number = 0;

  constructor(data: any = null) {
    super(data);

    if (data) {
      this.humidity = data.humidity ? parseFloat(data.humidity) : 0;
      this.temperature = data.temperature ? parseFloat(data.temperature) : 0;
      this.ph = data.ph ? parseFloat(data.ph) : 0;
      this.light = data.light ? parseFloat(data.light) : 0;
    }
  }
}

class LastOnline extends FirebaseData {
  timestamp: number = 0;

  constructor(data: any = null) {
    super(data);

    if (data) {
      this.timestamp = data.timestamp ? parseFloat(data.timestamp) : 0;
    }
  }
}

export default class UserPot extends FirebaseData {
  id: string | undefined;
  name: string | undefined;
  relayState: RelayState | undefined;
  schedule: CultureSchedule | undefined;
  sensors: Sensors | undefined;
  lastOnline: LastOnline | undefined;
  screenshotPath: string | undefined;
  photoRequest: PhotoRequest | undefined;

  constructor(data: any = null) {
    super(data);

    if (data) {
      this.relayState = data.relayState
        ? new RelayState(data.relayState)
        : new RelayState();
      this.schedule = data.schedule
        ? new CultureSchedule(data.schedule)
        : CultureSchedule.empty();
      this.sensors = data.sensors ? new Sensors(data.sensors) : new Sensors();
      this.lastOnline = new LastOnline(
        data.lastOnline ? data.lastOnline : null,
      );
      this.photoRequest = new PhotoRequest(data.photoRequest);
    }
  }

  get prettyHumidity() {
    return `${
      this.sensors && this.sensors.humidity ? this.sensors.humidity : 0
    }%`;
  }

  get prettyTemperature() {
    return `${
      this.sensors && this.sensors.temperature ? this.sensors.temperature : 0
    }°C`;
  }

  get prettyPh() {
    return `${this.sensors && this.sensors.ph ? this.sensors.ph : 0}`;
  }

  get prettyLight() {
    return `${this.sensors && this.sensors.light ? this.sensors.light : 0} Lx`;
  }
}

import FirebaseData from './FirebaseData';
import CultureTime from './CultureTime';

export default class CultureParameter extends FirebaseData {
  id: number = 0;
  startTime: CultureTime;
  fan: number;
  light: number;
  pump: number;
  photo: number;

  constructor(data?: any) {
    super(data);

    Object.assign(this, data);

    this.startTime =
      data && data.startTime
        ? new CultureTime(data.startTime)
        : CultureTime.createNew();
    this.fan = data && data.fan ? data.fan : 0;
    this.light = data && data.light ? data.light : 0;
    this.pump = data && data.pump ? data.pump : 0;
    this.photo = data && data.photo ? data.photo : 0;
  }

  compare(parameter: CultureParameter): number {
    return this.startTime.compare(parameter.startTime);
  }
}

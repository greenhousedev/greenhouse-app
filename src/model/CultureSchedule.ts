import FirebaseData from './FirebaseData';
import {CultureDay} from './CultureDay';
import _ from 'lodash';
import CultureTemplate from './CultureTemplate';

export default class CultureSchedule extends FirebaseData {
  static empty(): CultureSchedule {
    let s = new CultureSchedule();
    s.isEmpty = true;
    return s;
  }

  isEmpty: boolean = true;
  startedDate: number | undefined;
  potId: string | undefined;
  days: CultureDay[] = [];
  minTemperature: number | undefined;
  maxTemperature: number | undefined;

  constructor(data?: any = null) {
    super(data);

    Object.assign(this, data);

    if (data) {
      this.days = data.days
        ? _.toArray(data.days).map((value) => new CultureDay(value))
        : [];
    }
  }

  get durationDays() {
    if (!this.days) {
      return 0;
    }
    let sorted: CultureDay[] = CultureDay.sortDays(this.days);
    let last: CultureDay | undefined = _.last(sorted);
    if (!last) {
      return 0;
    }
    return last.dayIndex;
  }

  applyTemplate(template: CultureTemplate) {
    this.days = template.days;
    this.minTemperature = template.minTemperature;
    this.maxTemperature = template.maxTemperature;
  }
}

import FirebaseData from "./FirebaseData";

export class Pot extends FirebaseData {
    secret: string;
    used: boolean;
    userId: string;
}
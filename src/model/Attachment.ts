import FirebaseData from "./FirebaseData";

export class Attachment extends FirebaseData {

    url?: string;
    filename: string;

    constructor(data?: any) {
        super(data);

        Object.assign(this, data);
    }
}

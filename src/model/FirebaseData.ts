export default class FirebaseData {
  constructor(data: any = null) {
    if (data) {
      Object.assign(this, data);
    }
  }
}

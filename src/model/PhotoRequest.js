import FirebaseData from "./FirebaseData";
import ConfigManager from "../controller/ConfigManager";

export default class PhotoRequest extends FirebaseData {

    static init(): PhotoRequest {
        let r = new PhotoRequest();
        r.requested = true;
        r.requestTime = new Date().getTime();
        return r;
    }

    requestTime: number;
    requested: boolean;
    completed: boolean;
    processing: boolean;
    success: boolean;

    constructor(data: any = null) {
        super(data);

        this.requestTime = (data && data.requestTime) ?? 0;
        this.requested = (data && data.requested) ?? false;
        this.completed = (data && data.completed) ?? false;
        this.processing = (data && data.processing) ?? false;
        this.success = (data && data.success) ?? false;
    }

    get canTakePhoto(): boolean {
        if (this.completed === true) return true;
        if (this.hasTimeout) return true;
        //TODO remove test
        if(this.requested) return false;
        return this.processing !== true;
    }

    get hasTimeout() : boolean {
        return new Date().getTime() - this.requestTime > ConfigManager.appConfig.photoRequestTimeout
    }
}

import FirebaseData from './FirebaseData';
import {CultureDay} from './CultureDay';
import _ from 'lodash';
import {Attachment} from './Attachment';

export default class CultureTemplate extends FirebaseData {
  id: string = '';
  name: string = '';
  image?: string | null;
  days: CultureDay[] = [];
  minTemperature?: number | null;
  maxTemperature?: number | null;
  createdAt: number = 0;
  updatedAt: number = 0;
  observations?: string;
  attachments?: Attachment[];

  constructor(data: any = null) {
    super(data);

    Object.assign(this, data);

    if (data) {
      this.days = data.days
        ? _.toArray(data.days).map((value) => new CultureDay(value))
        : [];
      this.updatedAt = data.updatedAt ? parseFloat(data.updatedAt) : 0;
      this.attachments = data.attachments
        ? _.toArray(data.attachments).map((value) => new Attachment(value))
        : [];
    }
    /*console.log(
      'CultureTemplate::constructor  assigned updatedAt=' +
        this.updatedAt +
        ', data=' +
        JSON.stringify(data),
    );*/
  }

  sortDays() {
    this.days.sort((a, b) => a.compare(b));
    this.days.forEach((value) => value.sortParams());
  }

  compare(template: CultureTemplate): number {
    return template.updatedAt - this.updatedAt;

    /*if(this.updatedAt < template.updatedAt) return -1;
        if(this.updatedAt > template.updatedAt) return 1;
        return 0;*/
  }
}

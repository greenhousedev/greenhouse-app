import {RouteName} from './RouteName';

export type RootStackParamList = {
  [RouteName.Init]: undefined;
  [RouteName.Login]: undefined;
  [RouteName.LoggedIn]: undefined;
  [RouteName.Unconfirmed]: undefined;
};

export type LoggedInParamList = {
  [RouteName.MainTabs]: undefined;
  [RouteName.Plant]: undefined;
  [RouteName.Scan]: undefined;
  [RouteName.AddPlant]: undefined;
  [RouteName.Schedule]: undefined;
  [RouteName.AddTemplate]: undefined;
  [RouteName.Template]: undefined;
  [RouteName.PickTemplate]: undefined;

  [RouteName.Manual]: undefined;
};

export type GameRoomTabsParamList = {
  [RouteName.Greenhouses]: undefined;
  [RouteName.Templates]: undefined;
  [RouteName.Settings]: undefined;
};

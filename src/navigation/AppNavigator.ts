import {CommonActions, NavigationContainerRef} from '@react-navigation/native';
import {RootContainerType} from './RootContainer';

let appNavigator: NavigationContainerRef;
let appRootRef: RootContainerType;

function setTopLevelNavigator(navigatorRef: NavigationContainerRef) {
  appNavigator = navigatorRef;
}

function setRootContainerRef(rootRef: RootContainerType) {
  appRootRef = rootRef;
}

function resetToInit() {
  appRootRef?.resetToInit();
}

function goToLoggedIn() {
  appRootRef?.goToLoggedIn();
}

function navigate(name: string, params?: any) {
  if (appNavigator === null || appNavigator === undefined) {
    return;
  }

  appNavigator.dispatch(
    CommonActions.navigate({
      name,
      params,
    }),
  );
}

function back() {
  if (appNavigator === null || appNavigator === undefined) {
    return;
  }

  appNavigator.dispatch(CommonActions.goBack());
}

export default {
  setTopLevelNavigator,
  setRootContainerRef,
  resetToInit,
  goToLoggedIn,
  navigate,
  back,
};

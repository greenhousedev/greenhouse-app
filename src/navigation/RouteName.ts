export enum RouteName {
  Init = 'Init',
  Login = 'Login',

  LoggedIn = 'Logged In',
  Unconfirmed = 'Unconfirmed email',

  MainTabs = 'MainTabs',
  Greenhouses = 'Greenhouses',
  Templates = 'Templates',
  Settings = 'Settings',

  Plant = 'Plant',
  Scan = 'Scan',
  AddPlant = 'AddPlant',
  AddTemplate = 'AddTemplate',
  Template = 'Template',
  PickTemplate = 'PickTemplate',
  Schedule = 'Schedule',

  Manual = 'User guide',
}

import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import {InitScreenV2} from '../view/screen/InitScreen';
import React, {
  forwardRef,
  Ref,
  useCallback,
  useEffect,
  useImperativeHandle,
  useState,
} from 'react';
import {MainScreenV2} from '../view/screen/MainScreen';
import {LoginScreenV2} from '../view/screen/LoginScreen';
import {RouteName} from './RouteName';
import {LoggedInParamList, RootStackParamList} from './types';
import {FontSize, FontStyle, styleFont} from '../view/component/FuturaText';
import PlantScreen from '../view/screen/PlantScreen';
import TemplateScreen from '../view/screen/TemplateScreen';
import AddTemplateScreen from '../view/screen/AddTemplateScreen';
import {ScanScreen} from '../view/screen/ScanScreen';
import AddPlantScreen from '../view/screen/AddPlantScreen';
import ConfigManager from '../controller/ConfigManager';
import LoginManager from '../controller/LoginManager';
import PickTemplateScreen from '../view/screen/PickTemplateScreen';
import ScheduleScreen from '../view/screen/ScheduleScreen';
import ManualScreen from '../view/screen/ManualScreen';
import UserManager from '../controller/UserManager';
import UnconfirmedUserScreen from '../view/screen/UnconfirmedUserScreen';

type Props = {
  navigationRef: (ref: NavigationContainerRef) => any;
};

const LoggedInStack = createStackNavigator<LoggedInParamList>();

const headerStyle = {
  //backgroundColor: appTheme.colours.appBackgroundColor,
  //backgroundColor: 'red',
  elevation: 0,
  shadowColor: 'transparent',
  borderBottomWidth: 0,
};
const HeaderTitle = {
  ...styleFont(FontStyle.BOOK, FontSize.MEDIUM),
};

const AppHeader: StackNavigationOptions = {
  headerStyle: headerStyle,
  headerTitleStyle: HeaderTitle,
  headerTitleAlign: 'center',
  //headerTintColor: 'white',
  headerBackTitleVisible: false,
};

const AppNoHeader: StackNavigationOptions = {
  navigationOptions: {
    headerVisible: false,
  },
};

const LoggedInStackScreen = () => (
  <LoggedInStack.Navigator headerMode="screen" screenOptions={AppHeader}>
    <LoggedInStack.Screen name={RouteName.MainTabs} component={MainScreenV2} />
    <LoggedInStack.Screen name={RouteName.Plant} component={PlantScreen} />
    <LoggedInStack.Screen
      name={RouteName.Template}
      component={TemplateScreen}
    />
    <LoggedInStack.Screen
      name={RouteName.AddTemplate}
      component={AddTemplateScreen}
    />
    <LoggedInStack.Screen name={RouteName.Scan} component={ScanScreen} />
    <LoggedInStack.Screen
      name={RouteName.AddPlant}
      component={AddPlantScreen}
    />
    <LoggedInStack.Screen
      name={RouteName.PickTemplate}
      component={PickTemplateScreen}
    />
    <LoggedInStack.Screen
      name={RouteName.Schedule}
      component={ScheduleScreen}
    />
    <LoggedInStack.Screen name={RouteName.Manual} component={ManualScreen} />
  </LoggedInStack.Navigator>
);

export interface RootContainerType {
  resetToInit: () => any;
  goToLoggedIn: () => any;
}

const RootStack = createStackNavigator<RootStackParamList>();
const RootContainer = forwardRef(
  ({navigationRef}: Props, ref: Ref<RootContainerType>) => {
    const [_init, _setInit] = useState(true);

    const _startUp = () => {
      _setInit(true);
      console.log('RootContainer::start');
      ConfigManager.loadConfig(async (config) => {
        console.log('RootContainer::onResume config loaded');
        console.log(
          'RootContainer::onResume user confirmed=' + UserManager.confirmed,
        );
        if (LoginManager.isLoggedIn) {
          await UserManager.getUser();
          setTimeout(() => {
            _setInit(false);
          }, 2000);
        } else {
          _setInit(false);
        }
      });
    };

    useEffect(() => {
      _startUp();
    }, []);

    const resetToInit = useCallback(() => {
      _startUp();
    }, []);

    const goToLoggedIn = useCallback(() => {
      _startUp();
    }, []);

    useImperativeHandle(ref, () => ({resetToInit, goToLoggedIn}));

    return (
      <NavigationContainer ref={navigationRef}>
        <RootStack.Navigator headerMode={'none'}>
          {_init ? (
            <RootStack.Screen
              name={RouteName.Init}
              component={InitScreenV2}
              options={AppNoHeader}
            />
          ) : LoginManager.isLoggedIn ? (
            UserManager.confirmed ? (
              <RootStack.Screen
                name={RouteName.LoggedIn}
                component={LoggedInStackScreen}
              />
            ) : (
              <RootStack.Screen
                name={RouteName.Unconfirmed}
                component={UnconfirmedUserScreen}
              />
            )
          ) : (
            <RootStack.Screen
              name={RouteName.Login}
              component={LoginScreenV2}
            />
          )}
        </RootStack.Navigator>
      </NavigationContainer>
    );
  },
);
export default RootContainer;

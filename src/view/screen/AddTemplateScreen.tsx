import {BaseScreen} from './core/BaseScreen';
import {
  Alert,
  TextInput,
  View,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  FlatList, Platform,
} from 'react-native';
import DocumentPicker, {
  DocumentPickerResponse,
} from 'react-native-document-picker';
import getPath from '@flyerhq/react-native-android-uri-path';
import React, {useLayoutEffect, useState} from 'react';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {CommonStyles} from '../../style/CommonStyles';
import FuturaText, {FontSize, FontStyle} from '../component/FuturaText';
import {AppTheme} from '../../style/AppTheme';
import CoreButton from '../component/CoreButton';
import AppImagePicker from '../component/AppImagePicker';
import CultureTemplate from '../../model/CultureTemplate';
import UserManager from '../../controller/UserManager';
import TemplateScreen from './TemplateScreen';
import SimpleInput from '../component/SimpleInput';
import {CultureDay} from '../../model/CultureDay';
import storage from '@react-native-firebase/storage';
import AppNavigator from '../../navigation/AppNavigator';
import {RouteName} from '../../navigation/RouteName';
import CoreIconButton from '../component/CoreIconButton';
import {Icon} from 'react-native-elements';
import {Colors} from '../../style/Colors';
import {Attachment} from "../../model/Attachment";

const uuid = require('react-native-uuid');

export default function AddTemplateScreen({navigation, route}) {
  const [days, setDays] = useState<CultureDay[]>(route.params?.days ?? []);
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState('');
  const [observations, setObservations] = useState('');
  const [imagePath, setImagePath] = useState<string>();
  const [documents, setDocuments] = useState<DocumentPickerResponse[]>([]);

  const _onPhotoSelect = (imagePathNew: string | undefined) => {
    console.log(
      'AddTemplateScreen::onPhotoSelect image=' + JSON.stringify(imagePathNew),
    );
    setImagePath(imagePathNew);
  };

  const _onAddTemplate = async () => {
    if (!name || name.length === 0) {
      Alert.alert('', 'Please insert a name for the template!');
      return;
    }

    const template = new CultureTemplate();
    template.id = uuid.v1();
    template.observations = observations;
    template.name = name;
    template.days = days;

    setLoading(true);
    if (imagePath) {
      //upload photo to storage
      const sessionId = new Date().getTime();
      const imageRef = storage().ref('images').child(`${sessionId}`);
      let imageURL = await imageRef.getDownloadURL();
      console.log('AddTemplateScreen::onAddTemplate downloadURL: ' + imageURL);

      template.image = imageURL;
    }

    let attachments: Attachment[] = [];
    for (let d of documents) {
      const sessionId = new Date().getTime();
      const attachmentsRef = storage()
        .ref('attachments')
        .child(`${sessionId}_${template.id}`);
      let path = Platform.OS === 'android' ? getPath(d.uri) : d.uri;
      await attachmentsRef.putFile(path);
      let documentUrl = await attachmentsRef.getDownloadURL();
      console.log(
        'AddTemplateScreen::onAddTemplate documentUrl: ' + documentUrl,
      );

      if (documentUrl) {
        let a = new Attachment();
        a.filename = d.name;
        a.url = documentUrl;
        attachments.push(a);
      }
    }
    template.attachments = attachments;

    UserManager.addTemplate(template)
      .then((value) => {
        setLoading(false);
        AppNavigator.back();
        AppNavigator.navigate(RouteName.Template, {template});
        //AppNavigator.navigate(RouteName.Template, {template});
      })
      .catch((message) => {
        setLoading(false);
        Alert.alert('Error', message);
      });
  };

  const _onPressPickDocuments = async () => {
    // Pick multiple files
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });
      let docs: DocumentPickerResponse[] = documents.slice() ?? [];
      docs.push(...results);
      setDocuments(docs);
      for (const res of results) {
        console.log(
          res.uri,
          res.type, // mime type
          res.name,
          res.size,
        );
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitleStyle: AppTheme.headerTitleStyle,
      title: 'New Template',
    });
  }, [navigation]);

  let contentWidth =
    Dimensions.get('window').width - 2 * AppTheme.screen.padding;

  return (
    <View style={styles.screen}>
      {loading ? (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator color={AppTheme.primaryColor} size={'large'} />
        </View>
      ) : (
        <View style={{flex: 1}}>
          <FuturaText
            fontSize={FontSize.MEDIUM_LARGE}
            text={'Name your template'}
            style={{marginTop: AppTheme.screen.padding}}
          />

          <SimpleInput
            value={name}
            placeholder={'Name'}
            onChangeText={setName}
          />

          <SimpleInput
            value={observations}
            placeholder={'Observations'}
            onChangeText={setObservations}
          />

          <FuturaText
            fontSize={FontSize.MEDIUM_LARGE}
            text={'Add Photo'}
            style={{marginTop: AppTheme.screen.padding}}
          />

          <View style={{width: '100%', alignItems: 'center'}}>
            <AppImagePicker
              containerStyle={{alignSelf: 'center', marginTop: scale(4)}}
              onImageSelect={_onPhotoSelect}
              imagePath={imagePath}
              width={contentWidth}
              height={contentWidth}
              displayHeight={scale(120)}
            />
          </View>
          <FuturaText
            fontSize={FontSize.MEDIUM_LARGE}
            text={'Add Files'}
            style={{marginTop: AppTheme.screen.padding}}
          />

          <FlatList
            style={styles.documentsList}
            data={documents}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <DocumentListItem key={index.toString()} document={item} />
            )}
            ListFooterComponent={
              <CoreIconButton
                style={styles.addDocument}
                iconName={'plus'}
                onPress={_onPressPickDocuments}
              />
            }
            horizontal
          />

          <View style={{flex: 1}} />

          <CoreButton title={'Add template'} onPress={_onAddTemplate} />
        </View>
      )}
    </View>
  );
}

type ItemProps = {
  document: DocumentPickerResponse;
};

function DocumentListItem({document}: ItemProps) {
  return (
    <View style={styles.documentItem}>
      <Icon
        name={'attach'}
        type={'ionicon'}
        size={scale(30)}
        color={Colors.darkGrey}
      />
      <FuturaText
        text={document.name}
        numberOfLines={2}
        style={styles.documentName}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  screen: {
    ...CommonStyles.screenPadding,
  },

  documentsContainer: {},
  documentsList: {
    height: '70@s',
  },
  documentItem: {
    width: '70@s',
    height: '70@s',
    borderRadius: '8@s',
    borderWidth: '1@s',
    marginRight: '8@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addDocument: {
    width: '60@s',
    height: '60@s',
    borderRadius: '8@s',
    borderWidth: '1@s',
    marginTop: '5@s',
    borderColor: Colors.darkGrey,
  },
  documentName: {
    fontSize: '10@s',
    color: Colors.darkGrey,
    margin: '4@s',
  },
});

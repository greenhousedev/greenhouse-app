import {View, Alert} from 'react-native';
import React, {useEffect, useLayoutEffect, useState} from 'react';
import {ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CommonStyles} from '../../style/CommonStyles';
import UserManager from '../../controller/UserManager';
import {AppTheme} from '../../style/AppTheme';
import CultureTemplate from '../../model/CultureTemplate';
import {TemplateListView} from '../component/template/TemplateListView';
import AlertUtils from '../../utils/AlertUtils.ts';
import AppNavigator from '../../navigation/AppNavigator';
import {RouteName} from '../../navigation/RouteName';
import {useFocusEffect} from '@react-navigation/native';
import PotManager from '../../controller/PotManager';
import ShareTemplatePopup from '../component/template/ShareTemplatePopup';

export default function TemplatesScreen({navigation}) {
  const [templates, setTemplates] = useState<CultureTemplate[]>([]);
  const [loading, setLoading] = useState(false);
  const [activeTemplate, setActiveTemplate] = useState<CultureTemplate>();
  const [sharePopupVisible, setSharePopupVisible] = useState(false);

  const _loadTemplates = () => {
    setLoading(true);
    UserManager.getUser()
      .then((value) => {
        let templates =
          value && value.templates
            ? value.templates.slice().sort((a, b) => a.compare(b))
            : [];
        setTemplates(templates);
        setLoading(false);
      })
      .catch((reason) => {
        setLoading(false);
        Alert.alert('Error', reason);
      });
  };

  /* useEffect(() => {
    //onResume
    _loadTemplates();
  }, []);
*/

  useFocusEffect(
    React.useCallback(() => {
      _loadTemplates();
      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, []),
  );

  const _onPressTemplate = (template: CultureTemplate) => {
    AppNavigator.navigate(RouteName.Template, {template});
  };

  const _onPressNewTemplate = () => {
    AppNavigator.navigate(RouteName.AddTemplate);
  };

  const _onPressDeleteTemplate = (template: CultureTemplate, index: number) => {
    AlertUtils.yesNo(
      `Remove ${template.name}`,
      'Are you sure you want to remove this template?',
      () => {
        templates.splice(index, 1);
        setTemplates(templates);

        UserManager.removeTemplate(template)
          .then((value) => {
            _loadTemplates();
          })
          .catch((reason) => {});
      },
    );
  };

  const _onPressShare = (template: CultureTemplate) => {
    setActiveTemplate(template);
    setSharePopupVisible(true);
  };

  return (
    <View style={styles.screen}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={'large'} color={AppTheme.primaryColor} />
        </View>
      ) : (
        <TemplateListView
          templates={templates}
          onPressTemplate={_onPressTemplate}
          onPressNewTemplate={_onPressNewTemplate}
          onPressDeleteTemplate={_onPressDeleteTemplate}
          onPressShare={_onPressShare}
          canDelete={true}
          renderAddNew={false}
        />
      )}
      <ShareTemplatePopup
        template={activeTemplate}
        visible={sharePopupVisible}
        onClose={() => {
          setSharePopupVisible(false);
        }}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  screen: {
    flex: 1,
    //...CommonStyles.screen,
  },

  list: {
    //...CommonStyles.list,
  },

  emptyText: {
    color: AppTheme.primaryColor,
    flex: 1,
  },
});

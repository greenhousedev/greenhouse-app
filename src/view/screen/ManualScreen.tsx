import {ScaledSheet} from 'react-native-size-matters';
import {Dimensions, Platform, View} from 'react-native';
import Pdf from 'react-native-pdf';
import React from 'react';

const source =
  Platform.OS === 'android'
    ? {uri: 'bundle-assets://pdf/manual.pdf'}
    : {uri: 'bundle-assets://manual.pdf'};

export default function ManualScreen() {
  return (
    <View style={styles.container}>
      <Pdf
        source={source}
        onLoadComplete={(numberOfPages, filePath) => {
          console.log(`number of pages: ${numberOfPages}`);
        }}
        onPageChanged={(page, numberOfPages) => {
          console.log(`current page: ${page}`);
        }}
        onError={(error) => {
          console.log(error);
        }}
        onPressLink={(uri) => {
          console.log(`Link presse: ${uri}`);
        }}
        style={styles.pdf}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});

import React, {useCallback, useRef, useState} from 'react';
import {
  View,
  Alert,
  ImageBackground,
  StatusBar,
  ActivityIndicator,
  Image,
} from 'react-native';
import CoreInputV2, {CoreInputV2Type} from '../component/CoreInput';
import CoreButton from '../component/CoreButton';
import LoginManager from '../../controller/LoginManager';
import {ScaledSheet} from 'react-native-size-matters';
import {Assets} from '../../style/Assets';
import {AppTheme} from '../../style/AppTheme';
import SecondaryButton from '../component/SecondaryButton';
import UserManager from '../../controller/UserManager';
import AppNavigator from '../../navigation/AppNavigator';
import transformer from 'babel-jest';
import {RouteName} from '../../navigation/RouteName';
import {AppTitleText} from '../component/AppText';
import {
  FontSize,
  FontStyle,
  styleFont,
  styleFontStyle,
} from '../component/FuturaText';
import {Dimen} from '../../style/Dimen';

const EMAIL_REG = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

const MODE_LOGIN = 'login';
const MODE_SIGNUP = 'signup';
const MODE_FORGOT = 'forgot';

const INPUT_EMAIL = 'email';
const INPUT_PASSWORD = 'password';
const INPUT_PASSWORD_CONFIRM = 'passwordConfirm';

const checkInput = (id: string | undefined, text: string): string => {
  switch (id) {
    case INPUT_EMAIL:
      if (!text || text.length === 0) {
        return 'Cannot be empty';
      } else if (!EMAIL_REG.test(text)) {
        return 'Invalid email';
      }
      break;
    case INPUT_PASSWORD:
    case INPUT_PASSWORD_CONFIRM:
      if (!text || text.length < 6) {
        return 'Too short';
      }
      break;
  }
  console.log('LoginPopup::checkInput type ' + id + ' not recognized');
  return '';
};

export function LoginScreenV2() {
  const [mode, setMode] = useState(MODE_LOGIN);
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [passwordConfirmError, setPasswordConfirmError] = useState('');
  const [loading, setLoading] = useState(false);
  const passwordInput = useRef<CoreInputV2Type>(null);
  const passwordConfirmInput = useRef<CoreInputV2Type>(null);

  const _checkNextScreen = useCallback(() => {
    UserManager.getUser()
      .then((user) => {
        console.log(
          'Login::onLogin user.mainPotId=' +
            user.mainPotId +
            ', user.mainPot=' +
            user.mainPot,
        );
        //AppNavigator.navigate(RouteName.LoggedIn);
        AppNavigator.goToLoggedIn();
      })
      .catch((reason) => {
        Alert.alert('Error', reason);
      });
  }, []);

  const _onInputChange = useCallback((id: string | undefined, text: string) => {
    switch (id) {
      case INPUT_EMAIL:
        setEmail(text);
        setEmailError('');
        break;
      case INPUT_PASSWORD:
        setPassword(text);
        setPasswordError('');
        break;
      case INPUT_PASSWORD_CONFIRM:
        setPasswordConfirm(text);
        setPasswordConfirmError('');
        break;
    }
  }, []);

  const _onInputEnd = useCallback(
    (id: string | undefined, text: string) => {
      let error = checkInput(id, text);
      console.log(
        'LoginScreen::_onInputEnd id=' +
          id +
          ', text=' +
          text +
          ', error=' +
          error,
      );
      switch (id) {
        case INPUT_EMAIL:
          setEmail(text);
          setEmailError(error);
          break;
        case INPUT_PASSWORD:
          setPassword(text);
          setPasswordError(error);
          break;
        case INPUT_PASSWORD_CONFIRM:
          if (error.length === 0) {
            if (password !== text) {
              error = 'Passwords do not match';
            }
          }
          setPasswordConfirm(text);
          setPasswordConfirmError(error);
          break;
      }
    },
    [password],
  );

  const _onInputSubmitEditing = useCallback(
    (id: string, text: string) => {
      if (id === INPUT_EMAIL) {
        passwordInput.current?.focus();
      } else if (id === INPUT_PASSWORD && mode === MODE_SIGNUP) {
        passwordConfirmInput.current?.focus();
      }
    },
    [mode, passwordInput, passwordConfirmInput],
  );

  const _onPressLogin = useCallback(() => {
    //TODO do login
    console.log('LoginScreen::onPressLogin');
    let emailErrorNew = checkInput(INPUT_EMAIL, email);
    let passwordErrorNew = checkInput(INPUT_PASSWORD, password);
    setEmailError(emailErrorNew);
    setPasswordError(passwordErrorNew);

    if (emailErrorNew.length === 0 && passwordErrorNew.length === 0) {
      setLoading(true);
      LoginManager.loginEmail(email, password)
        .then((value) => {
          return UserManager.getUser();
        })
        .then((value) => {
          _checkNextScreen();
        })
        .catch((message) => {
          setLoading(false);
          Alert.alert('Login Error', message);
        });
    }
  }, [_checkNextScreen, email, password]);

  const _onPasswordReset = useCallback(() => {
    let emailErrorNew = checkInput(INPUT_EMAIL, email);
    setEmailError(emailErrorNew);

    if (emailErrorNew.length === 0) {
      setLoading(true);
      LoginManager.sendResetPassword(email)
        .then((value) => {
          setLoading(false);
          setMode(MODE_LOGIN);
          Alert.alert(
            'Check your email',
            'An email has been sent to you with instructions',
          );
        })
        .catch((message) => {
          setLoading(false);
          Alert.alert('Error', message);
        });
    }
  }, [email]);

  const _onSignUp = useCallback(() => {
    console.log('LoginScreen::onPressLogin');
    let emailErrorNew = checkInput(INPUT_EMAIL, email);
    let passwordErrorNew = checkInput(INPUT_PASSWORD, password);
    let passwordConfirmErrorNew = checkInput(
      INPUT_PASSWORD_CONFIRM,
      passwordConfirm,
    );
    if (passwordConfirmErrorNew.length === 0) {
      if (password !== passwordConfirm) {
        passwordConfirmErrorNew = 'Passwords do not match';
      }
    }
    setEmailError(emailErrorNew);
    setPasswordError(passwordErrorNew);
    setPasswordConfirmError(passwordConfirmErrorNew);

    if (
      emailErrorNew.length === 0 &&
      passwordErrorNew.length === 0 &&
      passwordConfirmErrorNew.length === 0
    ) {
      setLoading(true);
      LoginManager.signUp(email, password)
        .then((value) => {
          _checkNextScreen();
        })
        .catch((message) => {
          setLoading(false);
          Alert.alert('SignUp Error', message);
        });
    }
  }, [_checkNextScreen, email, password, passwordConfirm]);

  const _onGoToSignUp = useCallback(() => {
    setMode(MODE_SIGNUP);
  }, []);

  const _onForgotPassword = useCallback(() => {
    setMode(MODE_FORGOT);
  }, []);

  const _onGoToLogin = useCallback(() => {
    setMode(MODE_LOGIN);
  }, []);

  return (
    <ImageBackground style={styles.container} source={Assets.background.login}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator size={'large'} color={AppTheme.primaryColor} />
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={styles.siglaContainer}>
            <Image source={Assets.sigla.s3} style={styles.sigla} />
            <Image source={Assets.sigla.s1} style={styles.sigla} />
            <Image source={Assets.sigla.s2} style={styles.sigla} />
          </View>

          <AppTitleText text={'Memox Verdi'} style={styles.appTitle} />

          <View style={styles.contentContainer}>
            <CoreInputV2
              label={'Email'}
              style={styles.input}
              iconName={'envelope'}
              id={INPUT_EMAIL}
              text={email}
              error={emailError}
              keyboardType={'email-address'}
              onInputEnd={_onInputEnd}
              onInputChange={_onInputChange}
            />

            {mode !== MODE_FORGOT && (
              <CoreInputV2
                label={'Password'}
                style={styles.input}
                iconName={'lock'}
                ref={passwordInput}
                id={INPUT_PASSWORD}
                text={password}
                error={passwordError}
                secureTextEntry={true}
                hasNextInput={mode === MODE_SIGNUP}
                onInputEnd={_onInputEnd}
                onInputChange={_onInputChange}
              />
            )}

            {mode === MODE_SIGNUP && (
              <CoreInputV2
                label={'Confirm Password'}
                style={styles.input}
                iconName={'lock'}
                ref={passwordConfirmInput}
                id={INPUT_PASSWORD_CONFIRM}
                text={passwordConfirm}
                error={passwordConfirmError}
                secureTextEntry={true}
                onInputEnd={_onInputEnd}
                onInputChange={_onInputChange}
              />
            )}

            {mode !== MODE_FORGOT && (
              <SecondaryButton
                title={'Forgot password?'}
                onPress={_onForgotPassword}
                buttonStyle={[styles.buttonSecondary, styles.forgotPassword]}
              />
            )}

            {(mode === MODE_LOGIN || mode === MODE_SIGNUP) && (
              <View style={{flex: 1}} />
            )}

            {mode === MODE_LOGIN && (
              <CoreButton
                title={'Login'}
                onPress={_onPressLogin}
                style={styles.button}
              />
            )}
            {mode === MODE_SIGNUP && (
              <CoreButton
                title={'Sign Up'}
                onPress={_onSignUp}
                style={styles.button}
              />
            )}
            {mode === MODE_FORGOT && (
              <CoreButton
                title={'Request Password Reset'}
                onPress={_onPasswordReset}
                style={styles.button}
              />
            )}

            {mode === MODE_LOGIN && (
              <SecondaryButton
                title={"Don't have an account? Sign up"}
                onPress={_onGoToSignUp}
                buttonStyle={styles.buttonSecondary}
              />
            )}
            {mode === MODE_SIGNUP && (
              <SecondaryButton
                title={'Already have an account? Sign in'}
                onPress={_onGoToLogin}
                buttonStyle={styles.buttonSecondary}
              />
            )}

            {mode === MODE_FORGOT && (
              <SecondaryButton
                title={'Nevermind. Back to login'}
                onPress={_onGoToLogin}
                buttonStyle={styles.buttonSecondary}
              />
            )}

            <View
              style={{
                flex:
                  mode === MODE_FORGOT ? 1 : mode === MODE_LOGIN ? 0.27 : 0.5,
              }}
            />
          </View>
        </View>
      )}

      <StatusBar
        backgroundColor={'rgba(234, 234, 234, 0)'}
        translucent={true}
        barStyle={'dark-content'}
      />
    </ImageBackground>
  );
}

const styles = ScaledSheet.create({
  container: {
    backgroundColor: AppTheme.backgroundColor,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    //padding: '57@s',
  },

  contentContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    //paddingTop: '20@s',
    paddingHorizontal: '57@s',
    paddingBottom: '57@s',
  },

  input: {
    width: '100%',
    marginBottom: '10@s',
    //backgroundColor: '#94ff71',
  },

  button: {
    width: '100%',
    marginBottom: '10@s',
    backgroundColor: 'transparent',
  },

  buttonSecondary: {
    marginBottom: '10@s',
  },

  forgotPassword: {
    alignSelf: 'flex-end',
    marginRight: '14@s',
  },

  siglaContainer: {
    //marginHorizontal: '-57@s',
    width: '100%',
    //backgroundColor: 'red',
    flex: 0.5,
    paddingTop: '20@s',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    //backgroundColor: 'red',
  },

  sigla: {
    width: '80@s',
    height: '60@s',
    resizeMode: 'contain',
  },

  appTitle: {
    //backgroundColor: 'green',
    flex: 0.3,
    width: '100%',
    textAlign: 'center',
    textAlignVertical: 'center',
    ...styleFont(FontStyle.BOOK, FontSize.MEDIUM_LARGE),
    fontSize: Dimen.font.size.title,
  },
});

import React, {useCallback, useState} from 'react';
import {ActivityIndicator, Image, View} from 'react-native';
import {scale, ScaledSheet} from 'react-native-size-matters';
import {useFocusEffect} from '@react-navigation/native';
import {AppTheme} from '../../style/AppTheme';
import FuturaText, {
  FontSize,
  FontStyle,
  styleFontStyle,
} from '../component/FuturaText';
import {CommonStyles} from '../../style/CommonStyles';
import CoreButton from '../component/CoreButton';
import LoginManager from '../../controller/LoginManager';
import UserManager from '../../controller/UserManager';
import AppNavigator from '../../navigation/AppNavigator';
import Dialog, {DialogContent, DialogTitle} from 'react-native-popup-dialog';
import {Assets} from '../../style/Assets';
import User from '../../model/User';

export default function UnconfirmedUserScreen() {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState<User>();
  const [message, setMessage] = useState();

  useFocusEffect(
    useCallback(() => {
      setLoading(true);
      UserManager.getUser()
        .then((value) => {
          setUser(value);
          setLoading(false);
          if (UserManager.confirmed) {
            AppNavigator.goToLoggedIn();
          } else if (UserManager.confirmLink) {
            setLoading(true);
            UserManager.confirmUser()
              .then((value1) => {
                setLoading(false);
                AppNavigator.goToLoggedIn();
              })
              .catch((reason) => {
                setLoading(false);
              });
          }
        })
        .catch((reason) => {
          setLoading(false);
        });
    }, []),
  );

  const _onPressResend = () => {
    setLoading(true);
    LoginManager.resendConfirmationEmail()
      .then((value) => {
        setLoading(false);
        console.log('resendConfirmationEmail success');
        setMessage('A confirmation email has been sent to you.');
      })
      .catch((reason) => {
        setLoading(false);
        setMessage(reason);
        console.log('resendConfirmationEmail error=' + reason);
      });
  };

  const _onPressConfirmed = () => {
    UserManager.getUser()
      .then((value) => {
        AppNavigator.goToLoggedIn();
      })
      .catch((reason) => {
        setMessage(reason);
      });
  };

  const _onLogout = () => {
    LoginManager.logout()
      .then((value) => {
        AppNavigator.goToLoggedIn();
      })
      .catch((reason) => {
        setMessage(reason);
      });
  };

  const _onCloseMessage = () => {
    setMessage(undefined);
  };

  return (
    <View style={styles.screen}>
      <View style={{flex: 0.6}} />

      <Image source={Assets.icon.email} style={styles.emailIcon} />

      <FuturaText
        fontSize={FontSize.LARGE}
        fontStyle={FontStyle.MEDIUM}
        text={'Confirm your email address!'}
        style={styles.bigTitle}
      />

      <FuturaText
        text={'A confirmation email has been sent to'}
        style={styles.title}
      />
      <FuturaText
        fontStyle={FontStyle.MEDIUM}
        text={user?.email}
        style={styles.title}
      />

      <FuturaText
        text={
          "Check your inbox and click on the 'Confirm my email' link to confirm your email address."
        }
        style={[styles.title, {marginTop: scale(20)}]}
      />

      <View style={{flex: 1}} />
      <CoreButton
        title={'I have confirmed'}
        onPress={_onPressConfirmed}
        disabled={loading}
      />

      <View style={{flex: 1}} />
      {/*<SecondaryButton
        disabled={loading}
        buttonStyle={styles.secondary}
        textColor={AppTheme.primaryColor}
        title={'Resend confirmation email'}
        onPress={_onPressResend}
      />*/}
      <FuturaText
        text={'Did you not receive the email?'}
        style={[styles.title, {marginBottom: scale(10)}]}
      />

      <CoreButton
        title={'Resend confirmation email'}
        disabled={loading}
        onPress={_onPressResend}
      />

      <View style={{flex: 1}} />
      <CoreButton
        title={'Logout'}
        onPress={_onLogout}
        disabled={loading}
        style={styles.logout}
      />
      <Dialog
        rounded={true}
        visible={!!message}
        onHardwareBackPress={() => {
          _onCloseMessage();
          return true;
        }}
        onTouchOutside={() => {
          _onCloseMessage();
        }}>
        <DialogContent>
          <View
            style={{
              width: scale(220),
              height: scale(100),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <FuturaText text={message} style={styles.title} />
          </View>

          <CoreButton
            style={{marginTop: AppTheme.screen.padding}}
            title={'OK'}
            onPress={_onCloseMessage}
          />
        </DialogContent>
      </Dialog>
    </View>
  );
}

const styles = ScaledSheet.create({
  screen: {
    flex: 1,
    ...CommonStyles.screenPadding,
  },

  emailIcon: {
    alignSelf: 'center',
    width: '120@s',
    height: '100@s',
    tintColor: AppTheme.primaryColor,
  },

  bigTitle: {
    marginTop: '10@s',
    alignSelf: 'center',
    marginBottom: '20@s',
  },

  title: {
    alignSelf: 'center',
    textAlign: 'center',
  },

  list: {
    //...CommonStyles.list,
  },

  emptyText: {
    color: AppTheme.primaryColor,
    flex: 1,
  },

  secondary: {
    marginVertical: '20@s',
  },

  logout: {
    width: '200@s',
    alignSelf: 'center',
    marginBottom: '20@s',
  },
});

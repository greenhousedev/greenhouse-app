import {Alert, View} from 'react-native';
import React, {useCallback, useEffect, useState} from 'react';
import CoreButton from '../component/CoreButton';
import LoginManager from '../../controller/LoginManager';
import FuturaText, {FontSize} from '../component/FuturaText';
import {ScaledSheet} from 'react-native-size-matters';
import {CommonStyles} from '../../style/CommonStyles';
import UserManager from '../../controller/UserManager';
import {AppTheme} from '../../style/AppTheme';
import User from '../../model/User';
import AppNavigator from '../../navigation/AppNavigator';
import {RouteName} from '../../navigation/RouteName';

export default function SettingsScreen() {
  const [user, setUser] = useState<User | undefined>(undefined);

  const _onLogoutSuccess = useCallback(() => {
    console.log('SettingsScreen::onLogoutSuccess');
    //this.props.navigation.replace('Init');
    //AppNavigator.navigate(RouteName.Login);
    AppNavigator.resetToInit();
  }, []);

  const _onPressLogout = useCallback(() => {
    LoginManager.logout()
      .then((value) => {
        _onLogoutSuccess();
      })
      .catch((message) => {
        Alert.alert('Logout Error', message);
      });
  }, [_onLogoutSuccess]);

  useEffect(() => {
    UserManager.getUser()
      .then((user) => {
        setUser(user);
      })
      .catch((reason) => {});
  }, []);

  const _onPressManual = () => {
    AppNavigator.navigate(RouteName.Manual);
  };

  return (
    <View style={styles.screen}>
      <CoreButton
        style={styles.manual}
        title={'User guide'}
        onPress={_onPressManual}
      />

      {user && (
        <View>
          <FuturaText text={'You are logged in with:'} />
          <FuturaText
            text={user.email}
            fontSize={FontSize.MEDIUM}
            style={{color: AppTheme.primaryColor}}
          />
        </View>
      )}

      <CoreButton
        style={styles.logout}
        title={'Logout'}
        onPress={_onPressLogout}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  screen: {
    ...CommonStyles.screenPadding,
  },

  logout: {
    alignSelf: 'flex-start',
    marginTop: AppTheme.screen.padding,
    width: '200@s',
  },

  manual: {
    alignSelf: 'flex-start',
    marginTop: AppTheme.screen.padding,
    width: '100%',
    marginBottom: '40@s',
  },
});

import React, {useEffect} from 'react';
import {
  View,
  ActivityIndicator,
  ImageBackground,
  StatusBar,
  Text,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {AppTheme} from '../../style/AppTheme';
import LoginManager from '../../controller/LoginManager';
import {BaseFullscreenScreen} from './core/BaseFullscreenScreen';
import {Assets} from '../../style/Assets';
import PlantListScreen from './PlantListScreen';
import UserManager from '../../controller/UserManager';
import PlantScreen from './PlantScreen';
import ConfigManager from '../../controller/ConfigManager';
import AppNavigator from '../../navigation/AppNavigator';
import {RouteName} from '../../navigation/RouteName';

export function InitScreenV2() {

  //moved to root container
 /* useEffect(() => {
    console.log('InitScreen::start');
    ConfigManager.loadConfig((config) => {
      console.log('InitScreen::onResume config loaded');
      if (LoginManager.isLoggedIn) {
        setTimeout(() => {
          AppNavigator.navigate(RouteName.LoggedIn);
        }, 2000);
      } else {
        AppNavigator.navigate(RouteName.Login);
      }
    });
  }, []);*/

  return (
    <ImageBackground style={styles.container} source={Assets.background.login}>
      <ActivityIndicator color={AppTheme.primaryColor} size={'large'} />

      <StatusBar
        backgroundColor={'rgba(234, 234, 234, 0)'}
        translucent={true}
        barStyle={'light-content'}
      />
    </ImageBackground>
  );
}

/*
export default class InitScreen extends BaseFullscreenScreen {
  constructor(props) {
    super(props);

    console.log('InitScreen::constructor');

    //ConfigManager.appConfig;
  }

  render() {
    return (
       <ImageBackground
        style={styles.container}
        source={Assets.background.login}>
        <ActivityIndicator color={AppTheme.primaryColor} size={'large'} />

        <StatusBar
          backgroundColor={'rgba(234, 234, 234, 0)'}
          translucent={true}
          barStyle={'light-content'}
        />
      </ImageBackground>
    );
  }

  onResume() {
    super.onResume();
    console.log('InitScreen::onResume');
     ConfigManager.loadConfig((config) => {
      console.log('InitScreen::onResume config loaded');
      if (LoginManager.isLoggedIn) {
        setTimeout(() => {
          this.props.navigation.replace('Main');
        }, 1000);
      } else {
        this.props.navigation.replace('Login');
      }
    });
  }
}*/

const styles = ScaledSheet.create({
  container: {
    backgroundColor: AppTheme.backgroundColor,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

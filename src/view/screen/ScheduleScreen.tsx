import {BaseScreen} from './core/BaseScreen';
import {Alert, View, ActivityIndicator} from 'react-native';
import React, {useCallback, useState} from 'react';
import UserPot from '../../model/UserPot';
import FuturaText, {FontStyle} from '../component/FuturaText';
import CoreIconButton from '../component/CoreIconButton';
import {CommonStyles} from '../../style/CommonStyles';
import {CultureDay} from '../../model/CultureDay';
import UserManager from '../../controller/UserManager';
import CultureSchedule from '../../model/CultureSchedule';
import {ScaledSheet} from 'react-native-size-matters';
import {AppTheme} from '../../style/AppTheme';
import CultureDayListView from '../component/day/CultureDayListView';
import PotManager from '../../controller/PotManager';
import SecondaryButton from '../component/SecondaryButton';
import AppNavigator from '../../navigation/AppNavigator';

const moment = require('moment');

export default function ScheduleScreen({navigation, route}) {

  const {pot} = route.params;

  const [days, setDays] = useState<CultureDay[]>(
    pot && pot.schedule && pot.schedule.days
      ? CultureDay.sortDays(pot.schedule.days.slice())
      : [],
  );
  const [minTemperature, setMinTemperature] = useState(
    (pot && pot.schedule && pot.schedule.minTemperature) ?? null,
  );
  const [maxTemperature, setMaxTemperature] = useState(
    (pot && pot.schedule && pot.schedule.maxTemperature) ?? null,
  );
  const [loading, setLoading] = useState(false);

  const _saveSchedule = useCallback(() => {
    const {pot} = route.params;
    if (!pot.schedule) {
      pot.schedule = new CultureSchedule();
      pot.schedule.potId = pot.id;
    }
    pot.schedule.days = days;
    pot.schedule.minTemperature = minTemperature;
    pot.schedule.maxTemperature = maxTemperature;
    pot.schedule.startedDate = moment().unix();
    pot.schedule.isEmpty = false;

    PotManager.savePotSchedule(pot)
      .then((value) => {
        AppNavigator.back();
      })
      .catch((reason) => {
        Alert.alert('Error', reason);
      });
  }, [days, maxTemperature, minTemperature, route]);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: `${route.params.pot.name} Schedule`,
      headerTitleStyle: AppTheme.headerTitleStyle,
      headerRight: () => (
        <SecondaryButton
          buttonStyle={CommonStyles.rightNavButton}
          title={'Save'}
          textFontStyle={FontStyle.MEDIUM}
          textColor={AppTheme.primaryColor}
          onPress={_saveSchedule}
        />
      ),
    });
  }, [_saveSchedule, navigation, route.params.pot]);

  const _onDaysUpdate = (
    _days: CultureDay[],
    _minTemperature: number | null,
    _maxTemperature: number | null,
  ) => {
    setDays(CultureDay.sortDays(_days));
    setMinTemperature(_minTemperature);
    setMaxTemperature(_maxTemperature);
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={'large'} color={AppTheme.primaryColor} />
        </View>
      ) : (
        <CultureDayListView
          days={days}
          minTemperature={minTemperature}
          maxTemperature={maxTemperature}
          onDaysUpdate={_onDaysUpdate}
        />
      )}
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
    //paddingLeft: AppTheme.screen.padding,
    //paddingRight: AppTheme.screen.padding,
  },

  list: {
    flex: 1,
  },
});

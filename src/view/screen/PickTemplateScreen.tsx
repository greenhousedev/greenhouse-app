import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Alert, View} from 'react-native';
import UserManager from '../../controller/UserManager';
import {AppTheme} from '../../style/AppTheme';
import {TemplateListView} from '../component/template/TemplateListView';
import CultureTemplate from '../../model/CultureTemplate';
import {ScaledSheet} from 'react-native-size-matters';
import PotManager from '../../controller/PotManager';
import AppNavigator from '../../navigation/AppNavigator';

const moment = require('moment');

function PickTemplateScreen({navigation, route}) {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitleStyle: AppTheme.headerTitleStyle,
      title: 'Pick a Template',
    });
  }, [navigation]);

  const {pot} = route.params;
  const [loading, setLoading] = useState(false);
  const [templates, setTemplates] = useState<CultureTemplate[]>([]);

  useEffect(() => {
    setLoading(true);
    UserManager.getUser()
      .then((value) => {
        setLoading(false);
        setTemplates(value.templates ?? []);
      })
      .catch((reason) => {
        setLoading(false);
        Alert.alert('Error', reason);
      });
  }, []);

  const _onPressTemplate = (template: CultureTemplate) => {
    if (template) {
      setLoading(true);
      pot.schedule.startedDate = moment().unix();
      pot.schedule.applyTemplate(template);
      PotManager.savePotSchedule(pot)
        .then((value) => {
          AppNavigator.back();
        })
        .catch((reason) => {
          setLoading(false);
          Alert.alert('Error', reason);
        });
    }
  };


  return (<View style={styles.screen}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={'large'} color={AppTheme.primaryColor} />
        </View>
      ) : (
        <TemplateListView
          templates={templates}
          onPressTemplate={_onPressTemplate}
        />
      )}
    </View>
  );
}

const styles = ScaledSheet.create({
  screen: {
    flex: 1,
    // ...CommonStyles.listScreen,
  },

  list: {
    flex: 1,
  },

  emptyText: {
    color: AppTheme.primaryColor,
    flex: 1,
  },
});

export default PickTemplateScreen;

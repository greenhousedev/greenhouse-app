import {BaseScreen} from './core/BaseScreen';
import FuturaText from '../component/FuturaText';
import React, {useCallback, useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  View,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from 'react-native';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {ScanScreen} from './ScanScreen';
import {AppTheme} from '../../style/AppTheme';
import Swipeout, {SwipeoutButtonProperties} from 'react-native-swipeout';
import UserPot from '../../model/UserPot';
import {Colors} from '../../style/Colors';
import PlantScreen from './PlantScreen';
import StatusBarUtils from '../utils/StatusBarUtils';
import {Assets} from '../../style/Assets';
import {applySize, Dimen} from '../../style/Dimen';
import type {ImageSource} from 'react-native/Libraries/Image/ImageSource';
import PotManager from '../../controller/PotManager';
import CoreIconButton from '../component/CoreIconButton';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AlertUtils from '../../utils/AlertUtils.ts';
import NotificationsManager from '../../controller/NotificationsManager';
import AppNavigator from '../../navigation/AppNavigator';
import EmptyTitleView from '../component/EmptyTitleView';
import {RouteName} from '../../navigation/RouteName';

type ItemProps = {
  pot: UserPot;
  onPress: (pot: UserPot) => any;
  onPressDelete: (pot: UserPot) => any;
};

function PotListItem({pot, onPress, onPressDelete}: ItemProps) {
  const _onPressDelete = useCallback(() => {
    onPressDelete(pot);
  }, [onPressDelete, pot]);

  const _renderRelayState = useCallback(
    (prop: string, iconSource?: ImageSource) => {
      let relayState = pot.relayState;
      //let isOn = relayState && relayState[prop] === 1;
      // @ts-ignore
      let isOn = !relayState || relayState[prop] === 0;

      return (
        <Image
          style={{
            ...applySize(Dimen.iconSize.veryVerySmall),
            marginRight: scale(16),
            resizeMode: 'contain',
            tintColor: isOn ? AppTheme.primaryColor : Colors.grey,
          }}
          source={iconSource}
        />
      );
    },
    [pot],
  );

  const _onPress = useCallback(() => {
    onPress(pot);
  }, [onPress, pot]);

  let swipeOutButtons: SwipeoutButtonProperties[] = [
    {
      type: 'delete',
      backgroundColor: 'transparent',
      component: (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: AppTheme.alertColor,
            borderRadius: scale(6),
          }}>
          <CoreIconButton
            iconName={'delete'}
            overrideIcon={'material'}
            color={Colors.white}
            size={Dimen.iconSize.small}
            title={'Remove'}
            titleStyle={{color: Colors.white}}
            onPress={_onPressDelete}
          />
        </View>
      ),
    },
  ];

  return (
    <Swipeout
      right={swipeOutButtons}
      autoClose={true}
      backgroundColor={'transparent'}>
      <TouchableOpacity style={styles.listItemContainer} onPress={_onPress}>
        <View style={styles.listItemIconContainer}>
          <Image
            source={
              pot.screenshotPath ? {uri: pot.screenshotPath} : Assets.icon.pot
            }
            style={[
              styles.listItemIcon,
              pot.screenshotPath
                ? styles.listItemIconScreenshot
                : styles.listItemIconNoScreenshot,
            ]}
          />
        </View>
        <View style={{flex: 1}}>
          <FuturaText text={pot.name} style={styles.listItemTitle} />
          <View style={styles.listItemStatusContainer}>
            {_renderRelayState('light', Assets.icon.light)}
            {_renderRelayState('fan', Assets.icon.fan)}
            {_renderRelayState('pump', Assets.icon.pump)}

            <FuturaText
              text={pot.prettyTemperature}
              style={styles.listItemSensorText}
            />
            <View style={{flex: 1}} />
            <FuturaText
              text={pot.prettyHumidity}
              style={styles.listItemSensorText}
            />
            <View style={{flex: 1}} />
            <FuturaText text={pot.prettyPh} style={styles.listItemSensorText} />
            <View style={{flex: 1}} />
          </View>
        </View>

        <Icon name={'chevron-right'} style={{marginRight: scale(4)}} />
      </TouchableOpacity>
    </Swipeout>
  );
}

export default function PlantListScreen() {
  /*  static navigationOptions = {
    headerTitleStyle: AppTheme.headerTitleStyle,
    title: 'Greenhouses',
  };*/

  const [pots, setPots] = useState<UserPot[]>([]);
  const [loadingPots, setLoadingPots] = useState(false);

  const _onAddPlant = () => {
    AppNavigator.navigate('Scan');
  };

  const _onPressPotItem = useCallback((pot: UserPot) => {
    console.log('PlantListScreen::_onPressPotItem pot=' + JSON.stringify(pot));
    AppNavigator.navigate(RouteName.Plant, {pot});
  }, []);

  const _onPressDeletePotItem = (pot: UserPot) => {
    AlertUtils.yesNo(
      `Remove ${pot.name}`,
      'Are you sure you want to remove this greenhouse?',
      () => {
        PotManager.removePot(pot.id)
          .then((value) => {})
          .catch((reason) => {});
      },
    );
  };

  const _renderPotItem = useCallback(
    ({item}) => {
      return (
        <PotListItem
          pot={item}
          onPress={_onPressPotItem}
          onPressDelete={_onPressDeletePotItem}
        />
      );
    },
    [_onPressPotItem],
  );

  const _renderSeparator = () => {
    return <View style={{height: 1, backgroundColor: Colors.grey}} />;
  };

  useEffect(() => {
    console.log('PlantListScreen::useEffect onStart');
    let subscribeRef = PotManager.subscribeAllPots((potsNew) => {
      console.log(
        'PlantListScreen::subscribeAllPots ' + potsNew.length + ' pots',
      );
      setPots(potsNew);
      setLoadingPots(false);
    });
    NotificationsManager.start();
    return () => {
      console.log('PlantListScreen::useEffect onStop');
      NotificationsManager.stop();
      PotManager.unsubscribe(subscribeRef);
    };
  }, []);

  return (
    <View style={styles.container}>
      {StatusBarUtils.drawDarkStatus()}

      {loadingPots ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={'large'} color={AppTheme.primaryColor} />
        </View>
      ) : pots.length > 0 ? (
        <FlatList
          style={styles.list}
          data={pots}
          keyExtractor={(item, index) => item.id ?? index.toString()}
          renderItem={_renderPotItem}
          ItemSeparatorComponent={_renderSeparator}
          ListFooterComponent={_renderSeparator}
        />
      ) : (
        <EmptyTitleView
          title={
            "You don't have any plants added to your account. Press the '+' button to start the camera and scan a QR Code"
          }
        />
      )}
      {/* <CoreButton title={'Add a plant'} onPress={this._onAddPlant}/>*/}
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: AppTheme.screen.padding,
  },

  addNewButton: {
    backgroundColor: '#b4ff76',
  },

  list: {
    flex: 1,
  },

  listItemContainer: {
    height: '70@s',
    flexDirection: 'row',
    alignItems: 'center',
  },

  listItemTitle: {
    width: '100%',
    marginBottom: '12@s',
  },

  listItemIconContainer: {
    width: '50@s',
    height: '60@s',
    margin: '8@s',
    borderRadius: '10@s',
    backgroundColor: Colors.lightGrey,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },

  listItemIcon: {},

  listItemIconScreenshot: {
    width: '50@s',
    height: '60@s',
    resizeMode: 'cover',
  },

  listItemIconNoScreenshot: {
    width: '25@s',
    height: '50@s',
    resizeMode: 'contain',
  },

  listItemStatusContainer: {
    flexDirection: 'row',
  },

  listItemSensorText: {
    //backgroundColor: '#caff34',
    color: AppTheme.primaryColor,
    //width: '40@s',
    textAlign: 'center',
    fontSize: '14@s',
  },
});

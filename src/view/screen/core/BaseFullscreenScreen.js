import {BaseScreen} from './BaseScreen';

export class BaseFullscreenScreen extends BaseScreen {
  static navigationOptions = ({navigation}) => ({
    header: null,
  });
}

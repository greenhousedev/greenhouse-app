import React, {Component} from 'react';
import {Anims, AnimsUtils} from '../../../style/Anims';

export class BaseScreen extends Component {
  _isMounted: Boolean = false;

  componentDidMount(): void {
      console.log("BaseScreen::componentDidMount");
    this._isMounted = true;

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      (payload) => {
        this.onResume();
      },
    );
  }

  componentWillUnmount(): void {
    this._isMounted = false;

    this.willFocusSubscription.remove();
  }

  safeSetState(
    state: any,
    animate: boolean = false,
    layoutAnim: any = Anims.main,
  ) {
    if (!this._isMounted) {
      return;
    }
    if (animate && layoutAnim) {
      AnimsUtils.configureNext(layoutAnim);
    }
    this.setState(state);
  }

  onResume() {}
}

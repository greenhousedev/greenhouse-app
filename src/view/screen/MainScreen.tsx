import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import {Image} from 'react-native';
import {AppTheme} from '../../style/AppTheme';
import PlantListScreen from './PlantListScreen';
import {Assets} from '../../style/Assets';
import {applySize, Dimen} from '../../style/Dimen';
import {RouteName} from '../../navigation/RouteName';
import TemplatesScreen from './TemplatesScreen';
import SettingsScreen from './SettingsScreen';
import {CommonStyles} from '../../style/CommonStyles';
import AppNavigator from '../../navigation/AppNavigator';
import CoreIconButton from '../component/CoreIconButton';

const Tab = createBottomTabNavigator();

export function MainScreenV2({navigation, route}) {
  const getHeaderTitle = (route): String => {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "Feed" as that's the first screen inside the navigator
    return getFocusedRouteNameFromRoute(route) ?? RouteName.Greenhouses;
  };

  React.useLayoutEffect(() => {
    let focusedRoute = getHeaderTitle(route);
    console.log('MainScreen::useLayoutEffect focusedRoute=' + focusedRoute);
    navigation.setOptions({
      title: focusedRoute,
      headerTitleStyle: AppTheme.headerTitleStyle,
      headerRight: () =>
        focusedRoute === RouteName.Greenhouses ? (
          <CoreIconButton
            overrideIcon={'ionicon'}
            style={CommonStyles.rightNavButton}
            iconName={'md-add'}
            size={Dimen.iconSize.verySmall}
            onPress={() => AppNavigator.navigate(RouteName.Scan)}
          />
        ) : focusedRoute === RouteName.Templates ? (
          <CoreIconButton
            overrideIcon={'ionicon'}
            style={CommonStyles.rightNavButton}
            iconName={'md-add'}
            size={Dimen.iconSize.verySmall}
            onPress={() => AppNavigator.navigate(RouteName.AddTemplate)}
          />
        ) : undefined,
    });
  }, [navigation, route]);

  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: AppTheme.bottomBar.activeColor,
        inactiveTintColor: AppTheme.bottomBar.inactiveColor,

        tabStyle: {
          marginTop: AppTheme.bottomBar.tab.marginTop,
          marginBottom: AppTheme.bottomBar.tab.marginBottom,
        },
        style: {
          height: AppTheme.bottomBar.height,
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconSource = Assets.icon.home;
          if (route.name === RouteName.Greenhouses) {
          } else if (route.name === RouteName.Templates) {
            iconSource = Assets.icon.template;
          } else if (route.name === RouteName.Settings) {
            iconSource = Assets.icon.settings;
          }
          //return <Icon name={iconName} color={tintColor} size={AppTheme.bottomBar.iconSize}/>;
          return (
            <Image
              source={iconSource}
              style={{
                ...applySize(Dimen.iconSize.small),
                resizeMode: 'contain',
                tintColor: color,
              }}
            />
          );
        },
      })}>
      <Tab.Screen name={RouteName.Greenhouses} component={PlantListScreen} />
      <Tab.Screen name={RouteName.Templates} component={TemplatesScreen} />
      <Tab.Screen name={RouteName.Settings} component={SettingsScreen} />
    </Tab.Navigator>
  );
}

/*
export const MainScreen = createBottomTabNavigator(
  {
    Greenhouses: PlantListScreen,
    Templates: TemplatesScreen,
    Settings: SettingsScreen,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName = 'home';
        let iconSource = Assets.icon.home;
        if (routeName === 'Greenhouses') {
        } else if (routeName === 'Schedule') {
          iconSource = Assets.icon.schedule;
        } else if (routeName === 'Culture') {
          iconSource = Assets.icon.culture;
        } else if (routeName === 'Templates') {
          iconSource = Assets.icon.template;
        } else if (routeName === 'Settings') {
          iconName = 'settings';
          iconSource = Assets.icon.settings;
        }
        //return <Icon name={iconName} color={tintColor} size={AppTheme.bottomBar.iconSize}/>;
        return (
          <Image
            source={iconSource}
            style={{
              ...applySize(Dimen.iconSize.small),
              resizeMode: 'contain',
              tintColor: tintColor,
            }}
          />
        );
      },
    }),

    tabBarOptions: {
      activeTintColor: AppTheme.bottomBar.activeColor,
      inactiveTintColor: AppTheme.bottomBar.inactiveColor,

      tabStyle: {
        marginTop: AppTheme.bottomBar.tab.marginTop,
        marginBottom: AppTheme.bottomBar.tab.marginBottom,
      },
      style: {
        height: AppTheme.bottomBar.height,
      },
    },
  },
);

MainScreen.navigationOptions = ({navigation}) => {
  const {routeName} = navigation.state.routes[navigation.state.index];

  // You can do whatever you like here to pick the title based on the route name
  const config = {
    headerTitleStyle: AppTheme.headerTitleStyle,
    headerTitle: routeName,
  };
  if (routeName === 'Greenhouses') {
    config.headerRight = (
      <CoreIconButton
        overrideIcon={Icon}
        style={CommonStyles.rightNavButton}
        iconName={'md-add'}
        size={Dimen.iconSize.verySmall}
        onPress={() => ScanScreen.navigate(navigation)}
      />
    );
  } else if (routeName === 'Templates') {
    config.headerRight = (
      <CoreIconButton
        overrideIcon={Icon}
        style={CommonStyles.rightNavButton}
        iconName={'md-add'}
        size={Dimen.iconSize.verySmall}
        onPress={() => AddTemplateScreen.navigate(navigation)}
      />
    );
  }

  return config;
};
*/

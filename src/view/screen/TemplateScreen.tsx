import React, {useCallback, useState} from 'react';
import {View, Alert} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {AppTheme} from '../../style/AppTheme';
import CultureDayListView from '../component/day/CultureDayListView';
import {CultureDay} from '../../model/CultureDay';
import {FontStyle} from '../component/FuturaText';
import {CommonStyles} from '../../style/CommonStyles';
import UserManager from '../../controller/UserManager';
import SecondaryButton from '../component/SecondaryButton';
import CultureTemplate from '../../model/CultureTemplate';

export default function TemplateScreen({route, navigation}) {
  const t: CultureTemplate = route.params.template;
  console.log('TemplateScreen template=' + JSON.stringify(t));

  const [days, setDays] = useState<CultureDay[]>(
    t && t.days ? CultureDay.sortDays(t.days.slice()) : [],
  );
  const [minTemperature, setMinTemperature] = useState(t && t.minTemperature);
  const [maxTemperature, setMaxTemperature] = useState(t && t.maxTemperature);
  const [observations, setObservations] = useState(t && t.observations);

  const _saveTemplate = useCallback(() => {

    const template: CultureTemplate = {...route.params.template};
    console.log('TemplateScreen::_saveTemplate id:'+template.id);
    template.days = days;
    template.observations = observations;
    template.minTemperature = minTemperature;
    template.maxTemperature = maxTemperature;

    UserManager.saveTemplate(template)
      .then((value) => {
        Alert.alert('Success', 'Your template has been saved.');
      })
      .catch((reason) => {
        Alert.alert('Error', reason);
      });
  }, [
    days,
    maxTemperature,
    minTemperature,
    observations,
    route.params.template,
  ]);

  const _onDaysUpdate = useCallback(
    (
      daysNew: CultureDay[],
      minTemperatureNew?: number | null,
      maxTemperatureNew?: number | null,
      observationsNew?: string,
    ) => {
      console.log(
        'TemplateScreen::_onDaysUpdate observationsNew=' + observationsNew,
      );
      setDays(CultureDay.sortDays(daysNew).slice());
      setMinTemperature(minTemperatureNew);
      setMaxTemperature(maxTemperatureNew);
      setObservations(observationsNew);
    },
    [setMaxTemperature, setMinTemperature, setObservations],
  );

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: route.params.template.name,
      headerTitleStyle: AppTheme.headerTitleStyle,
      headerRight: () => (
        <SecondaryButton
          buttonStyle={CommonStyles.rightNavButton}
          title={'Save'}
          textFontStyle={FontStyle.MEDIUM}
          textColor={AppTheme.primaryColor}
          onPress={_saveTemplate}
        />
      ),
    });
  }, [_saveTemplate, navigation, route.params.template]);

  return (
    <View style={styles.container}>
      <CultureDayListView
        days={days}
        attachments={t.attachments}
        showObservations={true}
        observations={observations}
        minTemperature={minTemperature}
        maxTemperature={maxTemperature}
        onDaysUpdate={_onDaysUpdate}
        updatedAt={t.updatedAt}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
  },

  list: {
    flex: 1,
  },
});

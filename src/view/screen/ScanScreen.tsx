import {BaseScreen} from './core/BaseScreen';
import React, {useEffect} from 'react';
import {Alert, View} from 'react-native';
import {ScaledSheet, scale} from 'react-native-size-matters';
import FuturaText, {FontStyle} from '../component/FuturaText';
import QRCodeScanner from 'react-native-qrcode-scanner';
import AddPlantScreen from './AddPlantScreen';
import {AppTheme} from '../../style/AppTheme';
import SecondaryButton from '../component/SecondaryButton';
import {CommonStyles} from '../../style/CommonStyles';
import AppNavigator from '../../navigation/AppNavigator';
import {RouteName} from '../../navigation/RouteName';

export function ScanScreen({navigation, route}) {
  const _onScanSuccess = (e) => {
    console.log('ScanScreen::onScanSuccess data=' + JSON.stringify(e.data));
    if (e && e.data) {
      const parts = e.data.split('/');
      if (parts.length === 2) {
        AppNavigator.navigate(RouteName.AddPlant, {
          potId: parts[0],
          secret: parts[1],
        });
        return;
      }
    }
    Alert.alert('', 'Invalid QR Code. Please scan a pot!');
  };

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitleStyle: AppTheme.headerTitleStyle,
      title: 'Scan QR Code',
    });
  }, [navigation]);

  /*//TODO remove test
  useEffect(() => {
    setTimeout(() => {
      AppNavigator.navigate(RouteName.AddPlant, {
        secret: '2312312',
        potId: 'asdasd',
      });
    }, 3000);
  }, []);*/

  return (
    <View style={styles.container}>
      <QRCodeScanner
        onRead={_onScanSuccess}
        topContent={<FuturaText text={'Scan the QR code placed on the pot'} />}
        cameraStyle={{height: '100%'}}
        topViewStyle={{flex: 0, height: scale(60)}}
        bottomViewStyle={{flex: 0}}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

import {
  View,
  Image,
  Alert,
  Platform,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import StatusBarUtils from '../utils/StatusBarUtils';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {Assets} from '../../style/Assets';
import {Colors} from '../../style/Colors';
import {AppTheme} from '../../style/AppTheme';
import {InfoOption, SwitchOption} from '../component/CultureOptions';
import RNPopoverMenu from 'react-native-popover-menu';
import Icon from 'react-native-vector-icons/FontAwesome';
import FuturaText, {FontStyle, styleFontStyle} from '../component/FuturaText';
import {Dimen} from '../../style/Dimen';
import PotManager from '../../controller/PotManager';
import Dialog, {DialogContent, DialogTitle} from 'react-native-popup-dialog';
import SimpleInput from '../component/SimpleInput';
import CoreButton from '../component/CoreButton';
import CoreIconButton from '../component/CoreIconButton';
import ConfigManager from '../../controller/ConfigManager';
import AppNavigator from '../../navigation/AppNavigator';
import {useFocusEffect} from '@react-navigation/native';
import {RouteName} from '../../navigation/RouteName';
import UserPot from '../../model/UserPot';
import SecondaryButton from "../component/SecondaryButton";
import {CommonStyles} from "../../style/CommonStyles";

const moment = require('moment');

export default function PlantScreen({navigation, route}) {
  /* TODO
      static navigationOptions = ({navigation}) => {
        return {
          headerTitleStyle: AppTheme.headerTitleStyle,
          title: `${navigation.state.params.pot.name}`,
        }
      };
      */

  const [pot, setPot] = useState(route.params.pot);
  const [potNameInput, setPotNameInput] = useState(route.params.pot);
  const [loading, setLoading] = useState(false);
  const [namePickerVisible, setNamePickerVisible] = useState(false);
  const _scheduleRef = useRef();

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitleStyle: AppTheme.headerTitleStyle,
    });
  }, [navigation]);

  const _onRemovePot = useCallback(() => {
    setLoading(true);

    PotManager.removePot(pot.id)
      .then((value) => {
        setLoading(false);
        AppNavigator.back();
      })
      .catch((reason) => {
        setLoading(false);
        Alert.alert('Error', reason);
      });
  }, [pot]);

  const _removePot = useCallback(() => {
    Alert.alert(
      `Remove ${pot.name}`,
      'Are you sure you want to remove this greenhouse?',
      [
        {text: 'Yes', style: 'destructive', onPress: _onRemovePot},
        {text: 'No', style: 'cancel'},
      ],
      {cancelable: false},
    );
  }, [_onRemovePot, pot]);

  const checkPhotoUpdate = useCallback((pot) => {
    let timeout =
      pot.photoRequest.requestTime +
      ConfigManager.appConfig?.photoRequestTimeout -
      new Date().getTime() +
      100;
    console.log(
      'PlantScreen::checkPhotoUpdate hasTimeout=' +
        pot.photoRequest.hasTimeout +
        ', timeout=' +
        timeout,
    );
    if (!pot.photoRequest.hasTimeout) {
      setTimeout(() => {
        console.log('PlantScreen::checkPhotoUpdate finish');
        //if (!this._isMounted) return;
        //this.forceUpdate();
      }, timeout);
    }
  }, []);

  const _onPotUpdate = useCallback(
    (potNew: UserPot) => {
      setPot(potNew);
      checkPhotoUpdate(potNew);
    },
    [checkPhotoUpdate],
  );

  // on start
  useEffect(() => {
    //TODO
    //this.props.navigation.setParams({removePot: this._removePot});
    console.log('PlantScreen::useEffect onStart');
    const subscribePot = PotManager.subscribePot(route.params.pot.id, (pot) => {
      //console.log('PlantScreen::subscribePot ' + JSON.stringify(pot));
      _onPotUpdate(pot);
    });

    return () => {
      console.log('PlantScreen::useEffect onStop');
      PotManager.unsubscribe(subscribePot);
    };
  }, [_onPotUpdate, route]);

  // on resume
  useFocusEffect(
    React.useCallback(() => {
      console.log('PlantScreen::useFocusEffect callback');
      let pot = route.params.pot;
      if (pot && pot.id) {
        PotManager.getPot(pot.id)
          .then((pot) => {
            _onPotUpdate(pot);
          })
          .catch((reason) => {
            Alert.alert('Error', 'Could not read data. Please try again!');
          });
      }

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
      };
    }, [_onPotUpdate, route.params.pot]),
  );

  const _onOptionValueChange = (optionType: string, value: number) => {
    console.log(
      "PlantScreen::_onOptionValueChange '" +
        optionType +
        "' " +
        pot.relayState[optionType] +
        ' => ' +
        value,
    );
    pot.relayState[optionType] = value;
    pot.relayState = {...pot.relayState};
    setPot(pot);
    setTimeout(() => {
      PotManager.updateRelayState(pot, optionType, value)
        .then((value1) => {
          console.log('PlantScreen::_onOptionValueChange success: ');
        })
        .catch((reason) => {
          console.log('PlantScreen::_onOptionValueChange error: ' + reason);
        });
    }, 100);
  };
  const _doChangeName = useCallback(() => {
    setNamePickerVisible(false);

    PotManager.updateName(pot, potNameInput)
      .then((value) => {})
      .catch((reason) => {});
  }, [pot, potNameInput]);

  const _onPressTakePhoto = useCallback(() => {
    PotManager.requestPhoto(pot)
      .then((value) => {})
      .catch((reason) => {});
  }, [pot]);

  const _drawNamePopup = () => (
    <Dialog
      rounded={true}
      visible={namePickerVisible}
      width={0.9}
      dialogTitle={
        <DialogTitle
          title={'Insert new name'}
          textStyle={{...styleFontStyle()}}
        />
      }
      onHardwareBackPress={() => {
        setNamePickerVisible(false);
        return true;
      }}
      onTouchOutside={() => {
        setNamePickerVisible(false);
      }}>
      <DialogContent>
        <SimpleInput
          style={{marginTop: AppTheme.screen.padding}}
          autoFocus={true}
          value={`${potNameInput}`}
          onChangeText={setPotNameInput}
        />

        <CoreButton
          disabled={potNameInput.length === 0}
          style={{marginTop: AppTheme.screen.padding}}
          title={'Change name'}
          onPress={_doChangeName}
        />
      </DialogContent>
    </Dialog>
  );

  const _onNamePress = useCallback(
    (optionType: string) => {
      setNamePickerVisible(true);
      setPotNameInput(pot.name);
    },
    [pot],
  );

  const _getMenuIcon = (iconName: string) => (
    <Icon
      name={iconName}
      color={AppTheme.primaryColor}
      size={Dimen.iconSize.verySmall}
      family={'FontAwesome'}
    />
  );

  const _onPressSchedule = useCallback(
    (optionType: string) => {
      console.log("PlantScreen::_onPressSchedule");
      let hasSchedule = pot.schedule && !pot.schedule.isEmpty;
      let templateMenus = [
        {label: 'Load from template', icon: _getMenuIcon('copy')},
      ];
      if (hasSchedule) {
        templateMenus.push({
          label: 'Save as template',
          icon: _getMenuIcon('paste'),
        });
      }

      let menus = [
        {
          label: 'Schedule',
          menus: [
            {
              label: hasSchedule ? 'Edit' : 'Add',
              icon: _getMenuIcon(pot.schedule ? 'edit' : 'add'),
            },
          ],
        },
        {
          label: 'Template',
          menus: templateMenus,
        },
      ];


      console.log("PlantScreen::_onPressSchedule showing _scheduleRef="+_scheduleRef+", current="+_scheduleRef.current);
      if (!_scheduleRef || !_scheduleRef.current) return;
      RNPopoverMenu.Show(_scheduleRef.current, {
        title: '',
        menus: menus,
        onDone: (index, menuIndex) => {
          console.log(
            'PlantScreen::onDone index=' + index + ', menuIndex=' + menuIndex,
          );
          if (index === 0) {
            AppNavigator.navigate(RouteName.Schedule, {pot});
          } else if (
            (Platform.OS === 'ios' && index === 1) ||
            (Platform.OS === 'android' && index === 1 && menuIndex === 0)
          ) {
            //load template
            AppNavigator.navigate(RouteName.PickTemplate, {pot});
          } else if (
            (Platform.OS === 'ios' && index === 2) ||
            (Platform.OS === 'android' && index === 1 && menuIndex === 1)
          ) {
            //save template
            AppNavigator.navigate(RouteName.AddTemplate, {
              days: pot.schedule && pot.schedule.days,
            });
          }
        },
        onCancel: () => {},

        //ios only,
        menuWidth: scale(200),
        borderColor: Colors.grey,
        borderWidth: scale(1),
        separatorColor: Colors.grey,
      });
    },
    [pot],
  );

  //console.log('PlantScreen::render  this._scheduleRef=' + _scheduleRef);

  let hasStarted = pot.schedule && pot.schedule.startedDate && true;
  let totalDays = pot.schedule ? pot.schedule.durationDays : 0;

  let onlineTimeDiff =
    new Date().getTime() - (pot.lastOnline ? pot.lastOnline.timestamp : 0);
  let isOnline =
    -1 * 60 * 1000 < onlineTimeDiff && onlineTimeDiff < 5 * 60 * 1000;

  let passedDays = hasStarted
    ? moment().diff(moment.unix(pot.schedule.startedDate), 'days')
    : 0;
  let hasCompleted = passedDays > totalDays;
  /*  console.log(
    'PlantScreen::render startedDate=' +
      pot.schedule.startedDate +
      ' (' +
      moment.unix(pot.schedule.startedDate).format() +
      '), hasStarted=' +
      hasStarted +
      ', totalDays=' +
      totalDays +
      ', passedDays=' +
      passedDays +
      ', hasCompleted=' +
      hasCompleted,
  );*/

  /* console.log(
    'PlantScreen::render photoRequest=' + JSON.stringify(pot.photoRequest),
  );
*/
  const canTakePhoto = pot.photoRequest.canTakePhoto;
  /*console.log(
    'PlantScreen::render canTakePhoto=' +
      canTakePhoto +
      ', photoRequestTimeout=' +
      ConfigManager.appConfig?.photoRequestTimeout,
  );*/

  const relayState = pot ? pot.relayState : null;
  console.log('PlantScreen::render relayState=' + JSON.stringify(relayState));
  return (
    <ScrollView style={styles.container}>
      {StatusBarUtils.drawDarkStatus()}

      <View style={styles.statusContainer}>
        <FuturaText
          text={'Status'}
          fontStyle={FontStyle.MEDIUM}
          style={styles.statusLabel}
        />
        <FuturaText
          text={isOnline ? 'ONLINE' : 'OFFLINE'}
          fontStyle={FontStyle.MEDIUM}
          style={isOnline ? styles.statusOnline : styles.statusOffline}
        />
      </View>

      <View style={styles.imageContainer}>
        <View style={styles.imageWrapper}>
          <Image
            style={
              pot.screenshotPath
                ? styles.imageScreenshot
                : styles.imageNoScreenshot
            }
            source={
              pot.screenshotPath
                ? {uri: pot.screenshotPath}
                : Assets.icon.potShadow
            }
          />
        </View>
        {!canTakePhoto && (
          <View style={styles.photoLoadingContainer}>
            <ActivityIndicator size={'large'} color={AppTheme.primaryColor} />
          </View>
        )}
        <CoreIconButton
          disabled={!canTakePhoto}
          size={PHOTO_ICON_SIZE}
          iconName={'camera'}
          style={styles.photoButton}
          color={AppTheme.primaryColor}
          onPress={_onPressTakePhoto}
        />
      </View>

      <InfoOption
        optionType={'name'}
        label={'Name'}
        iconSource={Assets.icon.empty}
        iconColor={'transparent'}
        value={pot.name}
        valueActsAsButton={true}
        onPress={_onNamePress}
      />

      <SwitchOption
        optionType={'light'}
        label={'Light'}
        iconSource={Assets.icon.light}
        value={relayState ? relayState.light || 0 : 0}
        inverted={true}
        onValueChange={_onOptionValueChange}
      />
      <SwitchOption
        optionType={'fan'}
        label={'Fan'}
        iconSource={Assets.icon.fan}
        value={relayState ? relayState.fan || 0 : 0}
        inverted={true}
        onValueChange={_onOptionValueChange}
      />
      <SwitchOption
        optionType={'pump'}
        label={'Pump'}
        iconSource={Assets.icon.pump}
        value={relayState ? relayState.pump || 0 : 0}
        inverted={true}
        onValueChange={_onOptionValueChange}
      />

      <InfoOption
        optionType={'humidity'}
        label={'Humidity'}
        iconSource={Assets.icon.humidity}
        value={pot.prettyHumidity}
      />
      <InfoOption
        optionType={'temperature'}
        label={'Temperature'}
        iconSource={Assets.icon.temperature}
        value={pot.prettyTemperature}
      />

      <InfoOption
        optionType={'ph'}
        label={'PH'}
        iconSource={Assets.icon.ph}
        value={pot.prettyPh}
      />

      <InfoOption
        optionType={'light'}
        label={'Light Value'}
        iconSource={Assets.icon.light}
        value={pot.prettyLight}
      />

      <InfoOption
        optionType={'cultureDay'}
        label={'Culture Day'}
        iconSource={Assets.icon.template}
        value={
          !hasStarted
            ? 'Not started'
            : hasCompleted
            ? 'Finished'
            : `${passedDays}/${totalDays}`
        }
      />

      <InfoOption
        iconName={'calendar'}
        ref={_scheduleRef}
        optionType={'schedule'}
        label={'Schedule'}
        value={undefined}
        buttonIconName={'ellipsis-h'}
        onPress={_onPressSchedule}
      />

      {_drawNamePopup()}
    </ScrollView>
  );
}

const PHOTO_BUTTON_SIZE = scale(30);
const PHOTO_ICON_SIZE = scale(15);

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    paddingBottom: '16@s',
  },

  statusContainer: {
    marginTop: AppTheme.screen.padding,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  statusLabel: {
    marginRight: '8@s',
  },

  statusOnline: {
    color: AppTheme.primaryColor,
  },

  statusOffline: {
    color: AppTheme.alertColor,
  },

  imageContainer: {
    alignSelf: 'center',
    padding: PHOTO_BUTTON_SIZE / 2,
    // backgroundColor: '#69ff38',
    width: '190@s',
    height: '150@s',
    marginBottom: '24@s',
  },

  imageWrapper: {
    borderRadius: '10@s',
    backgroundColor: Colors.lightGrey,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    flex: 1,
  },

  imageScreenshot: {
    width: '100%',
    flex: 1,
    resizeMode: 'cover',
  },

  imageNoScreenshot: {
    width: '80@s',
    height: '100@s',
    resizeMode: 'contain',
  },

  optionContainer: {
    height: '40@s',
  },

  optionSeparator: {
    backgroundColor: Colors.grey,
    height: 1,
  },

  optionContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '40@s',
    marginRight: '20@s',
  },

  optionTitle: {
    flex: 1,
    marginLeft: '20@s',
  },

  optionValue: {
    marginLeft: '20@s',
    color: AppTheme.primaryColor,
  },

  photoButton: {
    position: 'absolute',
    right: 5,
    bottom: 5,
    width: PHOTO_BUTTON_SIZE,
    height: PHOTO_BUTTON_SIZE,
    backgroundColor: Colors.lightGrey,
    borderRadius: PHOTO_BUTTON_SIZE / 2,
  },

  photoLoadingContainer: {
    borderRadius: '10@s',
    backgroundColor: Colors.lightGreyTransparent,
    position: 'absolute',
    left: PHOTO_BUTTON_SIZE / 2,
    right: PHOTO_BUTTON_SIZE / 2,
    top: PHOTO_BUTTON_SIZE / 2,
    bottom: PHOTO_BUTTON_SIZE / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

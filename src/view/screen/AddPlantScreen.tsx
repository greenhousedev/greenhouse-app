import React, {useState} from 'react';
import {TextInput, View, ActivityIndicator, Alert} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {AppTheme} from '../../style/AppTheme';
import FuturaText, {
  FontSize,
  FontStyle,
  styleFontSize,
  styleFontStyle,
} from '../component/FuturaText';
import CoreButton from '../component/CoreButton';
import UserManager from '../../controller/UserManager';
import AppNavigator from '../../navigation/AppNavigator';

type FieldPresenterProps = {
  label: string;
  value: string;
};

function FieldPresenter(props: FieldPresenterProps) {
  return (
    <View style={styles.fieldContainer}>
      <FuturaText text={props.label} style={styles.fieldLabel} />
      <FuturaText
        fontSize={FontSize.MEDIUM_LARGE}
        text={props.value}
        style={styles.fieldValue}
      />
    </View>
  );
}

export default function AddPlantScreen({navigation, route}) {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitleStyle: AppTheme.headerTitleStyle,
      title: 'Add new greenhouse',
    });
  }, [navigation]);

  const [name, setName] = useState('');
  const [loading, setLoading] = useState(false);
  const {potId, secret} = route.params;

  const _onAddPlant = () => {
    setLoading(true);
    UserManager.addPotToUser(potId, secret, name)
      .then((value) => {
        setLoading(false);
        AppNavigator.back();
      })
      .catch((reason) => {
        setLoading(false);
        Alert.alert('Error', reason);
      });
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator color={AppTheme.primaryColor} size={'large'} />
        </View>
      ) : (
        <View style={{flex: 1}}>
          <FieldPresenter label={'Pot Id:'} value={potId} />
          <FieldPresenter label={'Secret:'} value={secret} />

          <FuturaText
            fontSize={FontSize.MEDIUM_LARGE}
            text={'Name your greenhouse'}
            style={{marginTop: AppTheme.screen.padding}}
          />

          <TextInput
            style={styles.input}
            value={name}
            placeholder={'Greenhouse Name'}
            onChangeText={setName}
          />
        </View>
      )}

      <CoreButton
        title={'Add greenhouse'}
        onPress={_onAddPlant}
        disabled={name.length === 0}
      />
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: AppTheme.screen.padding,
  },

  fieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  fieldLabel: {
    width: '65@s',
  },

  fieldValue: {
    color: AppTheme.primaryColor,
    flex: 1,
  },

  input: {
    ...styleFontStyle(FontStyle.BOOK),
    ...styleFontSize(FontSize.MEDIUM),
    width: '100%',
    borderBottomColor: AppTheme.primaryColor,
    borderBottomWidth: '1@s',
  },
});

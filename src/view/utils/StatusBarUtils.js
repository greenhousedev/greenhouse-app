import {StatusBar, View} from "react-native";
import {AppTheme} from "../../style/AppTheme";
import React from "react";

export default class StatusBarUtils {

    static drawDarkStatus() {
        return <StatusBar
            backgroundColor={AppTheme.backgroundColor}
            translucent={false}
            barStyle={"dark-content"}/>
    };
}

import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Header } from 'react-navigation';
import {Dimensions, Platform} from 'react-native';

export default class HeaderUtils {
    static getHeaderHeight = () => {
        if(Platform.OS === "ios") {
            const dim = Dimensions.get('window');
            if(HeaderUtils.isIPhoneXrSize(dim) || HeaderUtils.isIPhoneXSize(dim)) {
                return Header.HEIGHT - getStatusBarHeight()/2;
            }
            return Header.HEIGHT - getStatusBarHeight(); //70;

        } else if(Platform.OS === "android") {
            return Header.HEIGHT;
        }
        return 0;
    };

    // 44 - on iPhoneX
    // 20 - on iOS device
    // X - on Android platfrom (runtime value)
    // 0 - on all other platforms (default)
    static getStatusBarHeight = () => {
        return getStatusBarHeight()
    };

    static isIPhoneXSize = (dim) => {
        return dim.height === 812 || dim.width === 812;
    };

    static isIPhoneXrSize = (dim) => {
        return dim.height === 896 || dim.width === 896;
    };

}
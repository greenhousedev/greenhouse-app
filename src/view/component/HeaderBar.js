import React, {Component} from "react";
import {View} from 'react-native';
import {ScaledSheet} from "react-native-size-matters";
import {Header} from "react-navigation";

export class HeaderBar extends Component<{
    title: String,
}> {

    render() {
        return (<View style={styles.headerContainer}>

        </View>);
    }
}

const styles = ScaledSheet.create({
    headerContainer: {
        width: '100%',
        height: Header.HEIGHT,
        flexDirection: 'row',
        justifyContent: 'center',

    }
});
import React, {forwardRef, LegacyRef, Ref, useCallback} from 'react';
import type {ImageSource} from 'react-native/Libraries/Image/ImageSource';
import {
  Image,
  Switch,
  View,
  TouchableOpacity,
  StyleProp,
  ViewStyle,
} from 'react-native';
import {applySize, Dimen} from '../../style/Dimen';
import {AppTheme} from '../../style/AppTheme';
import FuturaText, {FontSize} from './FuturaText';
import {ScaledSheet} from 'react-native-size-matters';
import {Colors} from '../../style/Colors';
import CoreIconButton from './CoreIconButton';
import {AppLabelText, AppTitleText} from './AppText';
import {Icon, IconType} from "react-native-elements";

type SwitchOptionProps = {
  optionType: string;
  label?: string;
  iconName?: string;
  iconType?: IconType;
  iconSource?: ImageSource;
  contentStyle?: StyleProp<ViewStyle>;
  value: number;
  onValueChange: (optionType: string, value: number) => any;
  details?: string;
  detailsIsButton?: boolean;
  onDetailsPress?: (optionType: string) => any;
  inverted?: boolean;
};

export function SwitchOption(props: SwitchOptionProps) {
  const getTitle = () => {
    return props.label ?? props.optionType;
  };

  const _onValueChange = useCallback(
    (value: boolean) => {
      const onValue = props.inverted ? 0 : 1;
      const offValue = props.inverted ? 1 : 0;
      if (props.onValueChange) {
        props.onValueChange(props.optionType, value ? onValue : offValue);
      }
    },
    [props],
  );

  const _onPressDetails = () => {
    console.log('SwitchOption::_onPressDetails');
    if (props.onDetailsPress) {
      props.onDetailsPress(props.optionType);
    }
  };

  const detailsDisabled = props.detailsIsButton !== true;

  //console.log("SwitchOption::render detailsDisabled=" + detailsDisabled);

  return (
    <View style={styles.optionContainer}>
      <View style={[styles.optionContent, props.contentStyle]}>
        {props.iconName && (
          <Icon
            name={props.iconName}
            type={props.iconType}
            size={Dimen.iconSize.verySmall - 2}
            color={AppTheme.primaryColor}
          />
        )}
        {props.iconSource && (
          <Image
            style={{
              ...applySize(Dimen.iconSize.verySmall),
              resizeMode: 'contain',
              tintColor: AppTheme.primaryColor,
            }}
            source={props.iconSource}
          />
        )}
        <FuturaText text={getTitle()} style={styles.optionTitle} />
        {props.details && (
          <TouchableOpacity
            style={styles.detailsButton}
            disabled={detailsDisabled}
            onPress={_onPressDetails}>
            {detailsDisabled ? (
              <AppLabelText
                text={props.details}
                style={{paddingRight: AppTheme.screen.padding}}
              />
            ) : (
              <AppTitleText
                text={props.details}
                style={{paddingRight: AppTheme.screen.padding}}
              />
            )}
          </TouchableOpacity>
        )}
        <Switch
          trackColor={{true: AppTheme.primaryColor}}
          value={props.value === (props.inverted ? 0 : 1)}
          onValueChange={_onValueChange}
        />
      </View>
      <View style={styles.optionSeparator} />
    </View>
  );
}

type Props = {
  optionType: string;
  label?: string;
  iconName?: string;
  iconType?: IconType;
  iconSource?: ImageSource;
  iconColor?: any;
  value?: string;
  valueActsAsButton?: Boolean;
  buttonIconName?: string;
  onPress?: (optionType: string) => any;
  contentStyle?: StyleProp<ViewStyle>;
};

export const InfoOption = forwardRef((props: Props, ref: Ref<any>) => {
  const getTitle = () => {
    return props.label ?? props.optionType;
  };

  const _drawValue = useCallback(
    () => (
      <FuturaText
        fontSize={FontSize.MEDIUM_LARGE}
        text={props.value}
        style={styles.optionValue}
      />
    ),
    [props.value],
  );

  const _onPress = useCallback(() => {
    if (props.onPress) {
      props.onPress(props.optionType);
    }
  }, [props]);

  return (
    <View style={styles.optionContainer}>
      <View style={[styles.optionContent, props.contentStyle]}>
        {props.iconName && (
          <Icon
            name={props.iconName}
            type={props.iconType}
            size={Dimen.iconSize.verySmall}
            color={props.iconColor ?? AppTheme.primaryColor}
          />
        )}
        {props.iconSource && (
          <Image
            style={{
              ...applySize(Dimen.iconSize.verySmall),
              resizeMode: 'contain',
              tintColor: props.iconColor ?? AppTheme.primaryColor,
            }}
            source={props.iconSource}
          />
        )}
        <FuturaText
          text={getTitle()}
          style={styles.optionTitle}
          fontSize={FontSize.MEDIUM}
        />

        {props.value &&
          (props.valueActsAsButton ? (
            <TouchableOpacity onPress={_onPress}>
              {_drawValue()}
            </TouchableOpacity>
          ) : (
            _drawValue()
          ))}

        {props.buttonIconName && (
          <CoreIconButton
            ref={ref}
            iconName={props.buttonIconName}
            size={Dimen.iconSize.small}
            onPress={_onPress}
          />
        )}
      </View>
      <View style={styles.optionSeparator} />
    </View>
  );
});

const styles = ScaledSheet.create({
  optionContainer: {
    height: '36@s',
  },

  optionSeparator: {
    backgroundColor: Colors.grey,
    height: 1,
  },

  optionContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '40@s',
    marginRight: '20@s',
  },

  optionTitle: {
    flex: 1,
    marginLeft: '20@s',
  },

  optionValue: {
    marginLeft: '20@s',
    color: AppTheme.primaryColor,
  },

  detailsButton: {},
});

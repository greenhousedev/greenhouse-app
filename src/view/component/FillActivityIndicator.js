import React, {Component} from 'react';
import {ActivityIndicator, View} from "react-native";

import {CommonStyles} from "../../style/CommonStyles";
import {AppTheme} from "../../style/AppTheme";

export default class FillActivityIndicator extends Component<{
    backgroundColor?: string
}> {
    static WHITE_BACKGROUND: string = 'rgba(255, 255, 255, 0.4)';

    render() {
        return (
            <View style={{
                ...CommonStyles.fillParent,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: this.props.backgroundColor ?? FillActivityIndicator.WHITE_BACKGROUND
            }}>
                <ActivityIndicator size="large" color={AppTheme.primaryColor}/>
            </View>
        )
    }
}
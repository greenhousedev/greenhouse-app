import CultureTemplate from '../../../model/CultureTemplate';
import Dialog, {DialogContent, DialogTitle} from 'react-native-popup-dialog';
import {styleFontStyle} from '../FuturaText';
import SimpleInput from '../SimpleInput';
import {AppTheme} from '../../../style/AppTheme';
import {scale} from 'react-native-size-matters';
import CoreButton from '../CoreButton';
import React, {useEffect, useState} from 'react';
import UserManager from '../../../controller/UserManager';
import {ActivityIndicator, View} from 'react-native';
import {Colors} from '../../../style/Colors';
import {AppTitleText} from '../AppText';

type Props = {
  template?: CultureTemplate;
  visible: boolean;
  onClose: () => any;
};

export default function ShareTemplatePopup({
  template,
  visible,
  onClose,
}: Props) {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    if (visible) {
      //popup has become visible
      setFinished(false);
      setLoading(false);
    }
  }, [visible]);

  const _searchEmail = () => {
    if (email.length > 0) {
      setLoading(true);
      UserManager.findUserByEmail(email)
        .then((user) => {
          if (!template) {
            setMessage('Invalid template');
            setLoading(false);
            setFinished(true);
            return;
          }
          UserManager.shareTemplate(template, user.userId)
            .then((value) => {
              setMessage('Your template has been shared');
              setLoading(false);
              setFinished(true);
            })
            .catch((reason) => {
              setMessage(reason);
              setLoading(false);
              setFinished(true);
            });
        })
        .catch((reason) => {
          setMessage(reason);
          setLoading(false);
          setFinished(true);
        });
    }
  };

  return (
    <Dialog
      rounded={true}
      visible={visible}
      dialogTitle={
        <DialogTitle title="Share template" textStyle={{...styleFontStyle()}} />
      }
      onHardwareBackPress={() => {
        onClose();
        return true;
      }}
      onTouchOutside={() => {
        onClose();
      }}>
      <DialogContent>
        <View
          style={{
            width: scale(220),
            height: scale(60),
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {loading ? (
            <ActivityIndicator color={Colors.teal} />
          ) : finished ? (
            <AppTitleText text={message} style={{marginTop: scale(6)}} />
          ) : (
            <SimpleInput
              style={{marginTop: AppTheme.screen.padding, width: scale(200)}}
              autoFocus={true}
              keyboardType={'email-address'}
              value={email}
              onChangeText={setEmail}
            />
          )}
        </View>

        <CoreButton
          disabled={loading}
          style={{marginTop: AppTheme.screen.padding}}
          title={finished ? 'OK' : 'Share'}
          onPress={finished ? onClose : _searchEmail}
        />
      </DialogContent>
    </Dialog>
  );
}

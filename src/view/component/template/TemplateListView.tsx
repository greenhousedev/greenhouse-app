import {scale, ScaledSheet} from 'react-native-size-matters';
import CultureTemplate from '../../../model/CultureTemplate';
import {FlatList, ImageBackground, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FuturaText from '../FuturaText';
import CoreButton from '../CoreButton';
import {AppTheme} from '../../../style/AppTheme';
import {Colors} from '../../../style/Colors';
import React, {useCallback} from 'react';
import {CommonStyles} from '../../../style/CommonStyles';
import CoreIconButton from '../CoreIconButton';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {Dimen} from '../../../style/Dimen';
import Swipeout, {SwipeoutButtonProperties} from 'react-native-swipeout';
import EmptyTitleView from '../EmptyTitleView';
import moment from 'moment';

const LIST_ITEM_HEIGHT = scale(110);

type ItemProps = {
  template: CultureTemplate;
  index: number;
  canDelete?: boolean;
  onPress?: (template: CultureTemplate) => any;
  onPressDelete?: (template: CultureTemplate, index: number) => any;
  onPressShare: (template: CultureTemplate) => any;
};

//region ListItem
function TemplateListItem(props: ItemProps) {
  const _onPress = useCallback(() => {
    if (props.onPress) {
      props.onPress(props.template);
    }
  }, [props]);

  const _onPressDelete = useCallback(() => {
    if (props.onPressDelete) {
      props.onPressDelete(props.template, props.index);
    }
  }, [props]);

  let swipeOutButtons: SwipeoutButtonProperties[] = [
    {
      type: 'delete',
      backgroundColor: 'transparent',
      component: (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: AppTheme.alertColor,
            borderRadius: scale(6),
          }}>
          <CoreIconButton
            iconName={'delete'}
            overrideIcon={'material'}
            color={Colors.white}
            size={Dimen.iconSize.small}
            title={'Remove'}
            titleStyle={{color: Colors.white}}
            onPress={_onPressDelete}
          />
        </View>
      ),
    },
  ];

  const _onShare = () => {
    props.onPressShare(props.template);
  };

  return (
    <View style={styles.listItemContainer}>
      <Swipeout
        right={swipeOutButtons}
        autoClose={true}
        backgroundColor={'transparent'}
        disabled={!props.canDelete}>
        <View style={styles.listItemContent}>
          <TouchableOpacity style={styles.listItem} onPress={_onPress}>
            <ImageBackground
              style={styles.listItemImage}
              imageStyle={styles.listItemBackground}
              source={{uri: props.template.image}}>
              <View
                style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                }}>
                <LinearGradient
                  colors={['#00000015', '#00000015']}
                  start={{x: 0, y: 0}}
                  end={{x: 0, y: 1}}>
                  <View style={{width: '100%', height: LIST_ITEM_HEIGHT}} />
                </LinearGradient>
              </View>

              <View style={{flex: 1}} />

              <View style={styles.detailsContainer}>
                <FuturaText
                  text={props.template.name}
                  style={styles.listItemTitle}
                />
                {props.template.updatedAt && (
                  <FuturaText
                    text={moment(props.template.updatedAt).format(
                      'HH:mm DD/MM/yyyy',
                    )}
                    style={styles.listItemUpdateDate}
                  />
                )}
              </View>
            </ImageBackground>
          </TouchableOpacity>
          <View
            style={{
              position: 'absolute',
              right: scale(0),
              top: scale(0),
            }}>
            <CoreIconButton
              style={{padding: scale(14)}}
              overrideIcon={'ionicon'}
              iconName={'ios-share-social-sharp'}
              color={Colors.white}
              onPress={_onShare}
              size={scale(20)}
            />
          </View>
        </View>
      </Swipeout>
    </View>
  );
}

type Props = {
  templates?: CultureTemplate[];
  onPressNewTemplate?: () => any;
  onPressTemplate?: (template: CultureTemplate) => any;
  onPressDeleteTemplate?: (template: CultureTemplate, index: number) => any;
  onPressShare: (template: CultureTemplate) => any;
  canDelete?: boolean;
  renderAddNew?: boolean;
};

export function TemplateListView(props: Props) {
  return (
    <FlatList
      data={props.templates}
      style={CommonStyles.list}
      keyExtractor={(item) => item.id}
      contentContainerStyle={
        props.templates && props.templates.length > 0 ? undefined : {flex: 1}
      }
      renderItem={({item, index}) => (
        <TemplateListItem
          index={index}
          canDelete={props.canDelete}
          template={item}
          onPress={props.onPressTemplate}
          onPressShare={props.onPressShare}
          onPressDelete={props.onPressDeleteTemplate}
        />
      )}
      ListFooterComponent={
        props.renderAddNew ? (
          <CoreButton
            style={styles.addTemplateButton}
            title={'Add new template'}
            onPress={props.onPressNewTemplate}
          />
        ) : (
          <View style={{height: AppTheme.screen.padding}} />
        )
      }
      ListEmptyComponent={
        <EmptyTitleView
          title={
            "You have no templates saved. Press 'Add new template' to create a new one"
          }
        />
      }
    />
  );
}

const styles = ScaledSheet.create({
  listItemContainer: {
    marginTop: '24@s',
  },

  listItemContent: {
    height: LIST_ITEM_HEIGHT,
  },

  listItem: {
    borderRadius: '6@s',
    height: LIST_ITEM_HEIGHT,
    overflow: 'hidden',
  },

  addTemplateButton: {
    marginTop: AppTheme.screen.padding,
    marginBottom: AppTheme.screen.padding,
  },

  listItemImage: {
    height: LIST_ITEM_HEIGHT,
  },

  listItemBackground: {
    backgroundColor: AppTheme.primaryColor,
  },

  detailsContainer: {
    marginLeft: '24@s',
    marginRight: '12@s',
    //width: '100%',
    flexDirection: 'row',
    marginBottom: '14@s',
    alignItems: 'flex-end',
  },

  listItemTitle: {
    color: Colors.white,
    flex: 1,
  },

  listItemUpdateDate: {
    color: Colors.white,
    fontSize: '12@s',
  },
});

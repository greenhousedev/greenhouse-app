import * as React from 'react';
import {Text, StyleProp, TextStyle} from 'react-native';
import FuturaFontStyles from '../../style/FuturaFontStyles';
import {Dimen} from '../../style/Dimen';
import {ScaledSheet} from 'react-native-size-matters';
import {Colors} from '../../style/Colors';

export class FontStyle {
  static BOOK: FontStyle = new FontStyle();
  static MEDIUM: FontStyle = new FontStyle();

  FontStyle() {}
}

export class FontSize {
  static MICRO: FontSize = new FontSize();
  static VERY_SMALL: FontSize = new FontSize();
  static SMALL: FontSize = new FontSize();
  static MEDIUM: FontSize = new FontSize();
  static MEDIUM_LARGE: FontSize = new FontSize();
  static LARGE: FontSize = new FontSize();

  FontSize() {}
}

export type FuturaTextProps = {
  text: string | undefined;
  style?: StyleProp<TextStyle>;
  fontStyle?: FontStyle;
  fontSize?: FontSize;
  numberOfLines?: number;
};

export const styleFontStyle = (fontStyle?: FontStyle): any => {
  switch (fontStyle) {
    case FontStyle.BOOK:
      return FuturaFontStyles.book;
    case FontStyle.MEDIUM:
      return FuturaFontStyles.medium;
  }
  return FuturaFontStyles.book;
};

export const styleFontSize = (fontSize?: FontSize): any => {
  let size = Dimen.font.size.medium;
  switch (fontSize) {
    case FontSize.MICRO:
      size = Dimen.font.size.micro;
      break;
    case FontSize.VERY_SMALL:
      size = Dimen.font.size.verySmall;
      break;
    case FontSize.SMALL:
      size = Dimen.font.size.small;
      break;
    case FontSize.MEDIUM:
      size = Dimen.font.size.medium;
      break;
    case FontSize.MEDIUM_LARGE:
      size = Dimen.font.size.mediumLarge;
      break;
    case FontSize.LARGE:
      size = Dimen.font.size.large;
      break;
  }
  return {
    fontSize: size,
  };
};

export const styleFont = (fontStyle?: FontStyle, fontSize?: FontSize): any => {
  return {
    ...styleFontStyle(fontStyle),
    ...styleFontSize(fontSize),
  };
};

export default function FuturaText(props: FuturaTextProps) {
  let style = [
    styles.text,
    styleFontStyle(props.fontStyle),
    styleFontSize(props.fontSize),
    props.style,
  ];

  return (
    <Text style={style} numberOfLines={props.numberOfLines}>
      {props.text}
    </Text>
  );
}

/*export type FuturaTextProps = {
  text: string;
  style?: StyleSheet;
  fontStyle?: FontStyle;
  fontSize?: FontSize;
};

export default class FuturaText extends React.Component<FuturaTextProps> {
  render() {
    let style = [
      styles.text,
      FuturaText.styleFontStyle(this.props.fontStyle),
      FuturaText.styleFontSize(this.props.fontSize),
      this.props.style,
    ];

    return (
      <Text style={style} numberOfLines={this.props.numberOfLines}>
        {this.props.text}
      </Text>
    );
  }

  static styleFont(fontStyle?: FontStyle, fontSize?: FontSize): StyleSheet {
    return {
      ...FuturaText.styleFontStyle(fontStyle),
      ...FuturaText.styleFontSize(fontSize),
    };
  }

  static styleFontStyle(fontStyle?: FontStyle): StyleSheet {
    switch (fontStyle) {
      case FontStyle.BOOK:
        return FuturaFontStyles.book;
      case FontStyle.MEDIUM:
        return FuturaFontStyles.medium;
    }
    return FuturaFontStyles.book;
  }

  static styleFontSize(fontSize?: FontSize): StyleSheet {
    let size = Dimen.font.size.medium;
    switch (fontSize) {
      case FontSize.MICRO:
        size = Dimen.font.size.micro;
        break;
      case FontSize.VERY_SMALL:
        size = Dimen.font.size.verySmall;
        break;
      case FontSize.SMALL:
        size = Dimen.font.size.small;
        break;
      case FontSize.MEDIUM:
        size = Dimen.font.size.medium;
        break;
      case FontSize.MEDIUM_LARGE:
        size = Dimen.font.size.mediumLarge;
        break;
    }
    return {
      fontSize: size,
    };
  }
}*/

const styles = ScaledSheet.create({
  text: {
    color: Colors.darkGrey,
  },
});

import React, {forwardRef, Ref, useCallback, useState} from 'react';
import {Icon, IconType} from 'react-native-elements';
import {Colors} from '../../style/Colors';
import {AppTheme} from '../../style/AppTheme';
import type {Color} from 'react-native-vector-icons/index';
import {StyleProp, TextStyle, TouchableOpacity, ViewStyle} from 'react-native';
import type {PressEvent} from 'react-native/Libraries/Types/CoreEventTypes';
import {ScaledSheet} from 'react-native-size-matters';
import FuturaText, {FontSize} from './FuturaText';

type Props = {
  iconName: string;
  title?: string;
  color?: Color;
  pressedColor?: Color;
  onPress?: () => any;
  style?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  size?: number;
  overrideIcon?: IconType;
  disabled?: boolean;
};

const CoreIconButton = forwardRef((props: Props, ref: Ref<any>) => {
  const [isPressed, setIsPressed] = useState(false);

  const IconCustom = () => {
    return props.overrideIcon ?? Icon;
  };

  const _drawIcon = (size: number) => {
    return (
      <Icon
        type={props.overrideIcon ?? 'font-awesome'}
        name={props.iconName}
        color={
          props.disabled
            ? Colors.darkGrey
            : isPressed
            ? props.pressedColor ?? AppTheme.primaryColor
            : props.color ?? Colors.darkGrey
        }
        size={size}
        /* style={[styles.iconFontAwesome, {width: size}]}*/
      />
    );
  };

  const _onPressIn = useCallback((event: PressEvent) => {
    setIsPressed(true);
  }, []);

  const _onPressOut = useCallback((event: PressEvent) => {
    setIsPressed(false);
  }, []);

  return (
    <TouchableOpacity
      ref={ref}
      disabled={props.disabled}
      style={[styles.buttonContainer, props.style]}
      onPress={props.onPress}
      onPressIn={_onPressIn}
      onPressOut={_onPressOut}>
      {_drawIcon(props.size ?? AppTheme.input.iconSize)}
      {props.title && (
        <FuturaText
          text={props.title}
          style={[styles.title, props.titleStyle]}
          fontSize={FontSize.VERY_SMALL}
        />
      )}
    </TouchableOpacity>
  );
});

const styles = ScaledSheet.create({
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  iconFontAwesome: {
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: AppTheme.input.iconSize,
  },

  title: {},
});

export default CoreIconButton;

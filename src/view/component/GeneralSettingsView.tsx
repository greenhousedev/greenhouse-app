import React, {useState} from 'react';
import {View} from 'react-native';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {AppTitleText} from './AppText';
import {SwitchOption} from './CultureOptions';
import Dialog, {DialogContent, DialogTitle} from 'react-native-popup-dialog';
import {styleFontStyle} from './FuturaText';
import {AppTheme} from '../../style/AppTheme';
import CoreButton from './CoreButton';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import SimpleInput from './SimpleInput';

type GeneralSettingsViewProps = {
  observations?: string;
  showsObservations?: boolean;
  minTemperature?: number | null;
  maxTemperature?: number | null;
  onUpdate: (
    minTemperature?: number | null,
    maxTemperature?: number | null,
    observations?: string,
  ) => any;
};

const MIN = 'min';
const MAX = 'max';

export default function GeneralSettingsView(props: GeneralSettingsViewProps) {
  const [minValue, setMinValue] = useState(props.minTemperature ? 1 : 0);
  const [maxValue, setMaxValue] = useState(props.maxTemperature ? 1 : 0);
  const [minTemperature, setMinTemperature] = useState(props.minTemperature);
  const [maxTemperature, setMaxTemperature] = useState(props.maxTemperature);
  const [observations, setObservations] = useState(props.observations);
  const [popupInput, setPopupInput] = useState<string | null>(null);
  const [popupText, setPopupText] = useState('');

  const _onValueChange = (optionType: string, value: number) => {
    //console.log("GeneralSettingsView::_onValueChange optionType=" + optionType + ", value=" + value);

    const minValueNew = optionType === MIN ? value : minValue;
    const maxValueNew = optionType === MAX ? value : maxValue;
    const minTemperatureNew =
      optionType === MIN ? (value === 1 ? 0 : null) : minTemperature;
    const maxTemperatureNew =
      optionType === MAX ? (value === 1 ? 0 : null) : maxTemperature;
    const popupInputNew = value === 1 ? optionType : null;
    const popupTextNew =
      optionType === MIN
        ? minTemperature
          ? minTemperature.toString()
          : ''
        : maxTemperature
        ? maxTemperature.toString()
        : '';

    setMinValue(minValueNew);
    setMaxValue(maxValueNew);
    setMinTemperature(minTemperatureNew);
    setMaxTemperature(maxTemperatureNew);
    setPopupInput(popupInputNew);
    setPopupText(popupTextNew);

    if (value === 0) {
      // use callback only on disable temperature
      if (props.onUpdate) {
        props.onUpdate(minTemperatureNew, maxTemperatureNew, observations);
      }
    }
  };

  const _setObservations = (text: string) => {
    setObservations(text);
    if (props.onUpdate) {
      props.onUpdate(minTemperature, maxTemperature, observations);
    }
  };

  const _onPressDetails = (optionType: string) => {
    _openPopup(optionType);
  };

  const _openPopup = (optionType: string) => {
    const popupTextNew = optionType === MIN ? minTemperature : maxTemperature;
    setPopupInput(optionType);
    setPopupText(popupTextNew ? popupTextNew.toString() : '');
  };

  const _renderPopup = () => {
    const value = popupText;
    const popupVisible = !!popupInput;
    console.log(
      'GeneralSettingsView::renderPopup popupVisible=' +
        popupVisible +
        ', popupType=' +
        popupInput,
    );
    return (
      <Dialog
        rounded={true}
        visible={popupVisible}
        dialogTitle={
          <DialogTitle
            title="Insert temperature"
            textStyle={{...styleFontStyle()}}
          />
        }
        onHardwareBackPress={() => {
          setPopupInput(null);
          // use the callback on cancel insert temperature value
          if (props.onUpdate) {
            props.onUpdate(minTemperature, maxTemperature);
          }
          return true;
        }}
        onTouchOutside={() => {
          setPopupInput(null);
          // use the callback on cancel insert temperature value
          if (props.onUpdate) {
            props.onUpdate(minTemperature, maxTemperature);
          }
        }}>
        <DialogContent>
          <SimpleInput
            style={{marginTop: AppTheme.screen.padding, width: scale(120)}}
            autoFocus={true}
            keyboardType={'number-pad'}
            value={value}
            onChangeText={_onInputTemperatureChanged}
          />

          <CoreButton
            disabled={!value || value.length === 0}
            style={{marginTop: AppTheme.screen.padding}}
            title={'Apply temperature'}
            onPress={_onApplyTemperature}
          />
        </DialogContent>
      </Dialog>
    );
  };

  const _onInputTemperatureChanged = (text: string) => {
    setPopupText(text);
  };

  const _onApplyTemperature = () => {
    const minTemperatureNew =
      popupInput === MIN ? Number(popupText) : minTemperature;
    const maxTemperatureNew =
      popupInput === MAX ? Number(popupText) : maxTemperature;
    console.log(
      'GeneralSettingsView::_onApplyTemperature minTemperature=' +
        minTemperatureNew +
        ', maxTemperature=' +
        maxTemperatureNew,
    );

    setPopupInput(null);
    setMinTemperature(minTemperatureNew);
    setMaxTemperature(maxTemperatureNew);

    // use the callback on insert temperature value
    if (props.onUpdate) {
      props.onUpdate(minTemperatureNew, maxTemperatureNew);
    }
  };

  /*console.log("GeneralSettingsView::render minValue=" + minValue +
      ", maxValue=" + maxValue +
      ", minTemperature=" + minTemperature +
      ", maxTemperature=" + maxTemperature);*/

  return (
    <View style={styles.container}>
      <AppTitleText text={'General Settings'} />
      {props.showsObservations && (
        <SimpleInput
          value={observations}
          placeholder={'Observations'}
          onChangeText={_setObservations}
        />
      )}
      <SwitchOption
        optionType={MIN}
        value={minValue}
        label={'Min temperature'}
        contentStyle={styles.parameterOptionContent}
        onValueChange={_onValueChange}
        details={`${minTemperature || 0} °C`}
        detailsIsButton={
          minTemperature !== null && minTemperature !== undefined
        }
        onDetailsPress={_onPressDetails}
        iconName={'temperature-low'}
        iconType={'font-awesome-5'}
      />
      <SwitchOption
        optionType={MAX}
        value={maxValue}
        label={'Max temperature'}
        contentStyle={styles.parameterOptionContent}
        onValueChange={_onValueChange}
        details={`${maxTemperature || 0} °C`}
        detailsIsButton={
          maxTemperature !== null && maxTemperature !== undefined
        }
        onDetailsPress={_onPressDetails}
        iconName={'temperature-high'}
        iconType={'font-awesome-5'}
      />

      {_renderPopup()}
    </View>
  );
}

const styles = ScaledSheet.create({
  container: {
    margin: AppTheme.screen.padding,
  },

  parameterOptionContent: {
    marginLeft: '5@s',
    marginRight: '5@s',
  },
});

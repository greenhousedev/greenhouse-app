import React, {Component, useState} from 'react';
import {
  View,
  FlatList,
  Modal,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  Linking,
} from 'react-native';
import {CultureDay} from '../../../model/CultureDay';
import CultureTime from '../../../model/CultureTime';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {InfoOption, SwitchOption} from '../CultureOptions';
import CultureParameter from '../../../model/CultureParameter';
import CoreButton from '../CoreButton';
import FuturaText, {styleFontStyle} from '../FuturaText';
import {AppTheme} from '../../../style/AppTheme';
import Dialog, {DialogContent, DialogTitle} from 'react-native-popup-dialog';
import SimpleInput from '../SimpleInput';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {Colors} from '../../../style/Colors';
import {Assets} from '../../../style/Assets';
import SecondaryButton from '../SecondaryButton';
import Swipeout, {SwipeoutButtonProperties} from 'react-native-swipeout';
import CoreIconButton from '../CoreIconButton';
import {Dimen} from '../../../style/Dimen';
import AlertUtils from '../../../utils/AlertUtils';
import GeneralSettingsView from '../GeneralSettingsView';
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import {Attachment} from '../../../model/Attachment';
import {PermissionUtils} from '../../../utils/PermissionUtils';
import {AppTitleText} from '../AppText';

const uuid = require('react-native-uuid');

type ItemProps = {
  parameter: CultureParameter;
  index: number;
  onParameterUpdate: (index: number, parameter: CultureParameter) => any;
  onParameterDelete: (index: number) => any;
};

//region ParameterListItem
function ParameterListItem(props: ItemProps) {
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);

  const _onSwitchValueChange = (optionType: string, value: number) => {
    const {parameter} = props;
    switch (optionType) {
      case 'fan':
        parameter.fan = value;
        break;
      case 'light':
        parameter.light = value;
        break;
      case 'pump':
        parameter.pump = value;
        break;
      case 'photo':
        parameter.photo = value;
        break;
    }
    props.onParameterUpdate(props.index, parameter);
  };

  const _onPressTime = () => {
    setIsDateTimePickerVisible(true);
  };

  const _onConfirmTime = (date: Date) => {
    setIsDateTimePickerVisible(false);

    let param = props.parameter;
    param.startTime = CultureTime.createNew(date.getHours(), date.getMinutes());

    props.onParameterUpdate(props.index, param);
  };

  const _onCancelTime = (date: Date) => {
    setIsDateTimePickerVisible(false);
  };

  //region TimePicker
  const _drawTimePicker = () => (
    <DateTimePicker
      mode={'time'}
      isVisible={isDateTimePickerVisible}
      onConfirm={_onConfirmTime}
      onCancel={_onCancelTime}
    />
  );

  const _onPressDelete = () => {
    AlertUtils.yesNo(
      `Remove Parameters ${props.parameter.startTime.toPrettyText()}`,
      'Are you sure you to remove this culture day parameter ?',
      () => {
        props.onParameterDelete(props.index);
      },
    );
  };

  let swipeOutButtons: SwipeoutButtonProperties[] = [
    {
      type: 'delete',
      backgroundColor: AppTheme.alertColor,
      component: (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <CoreIconButton
            iconName={'delete'}
            overrideIcon={'material'}
            color={Colors.white}
            size={Dimen.iconSize.small}
            title={'Remove'}
            titleStyle={{color: Colors.white}}
            onPress={_onPressDelete}
          />
        </View>
      ),
    },
  ];

  const {parameter} = props;
  return (
    <View style={{marginBottom: AppTheme.screen.padding}}>
      <Swipeout
        right={swipeOutButtons}
        backgroundColor={AppTheme.alertColor}
        autoClose={true}>
        <View style={styles.parameterContent}>
          <InfoOption
            optionType={'time'}
            label={'Start Time'}
            contentStyle={styles.parameterOptionContent}
            iconSource={Assets.icon.time}
            value={parameter.startTime.toPrettyText()}
            valueActsAsButton={true}
            onPress={_onPressTime}
          />
          <SwitchOption
            optionType={'fan'}
            iconSource={Assets.icon.fan}
            contentStyle={styles.parameterOptionContent}
            value={parameter.fan}
            onValueChange={_onSwitchValueChange}
            inverted={true}
          />
          <SwitchOption
            optionType={'light'}
            iconSource={Assets.icon.light}
            contentStyle={styles.parameterOptionContent}
            value={parameter.light}
            onValueChange={_onSwitchValueChange}
            inverted={true}
          />
          <SwitchOption
            optionType={'pump'}
            iconSource={Assets.icon.pump}
            contentStyle={styles.parameterOptionContent}
            value={parameter.pump}
            onValueChange={_onSwitchValueChange}
            inverted={true}
          />

          <SwitchOption
            iconName={'camera'}
            optionType={'photo'}
            contentStyle={styles.parameterOptionContent}
            value={parameter.photo}
            onValueChange={_onSwitchValueChange}
          />

          {_drawTimePicker()}
        </View>
      </Swipeout>
    </View>
  );

  //endregion
}

//endregion

//region DayListItem
type DayItemProps = {
  day: CultureDay;
  index: number;
  onDayUpdate: (index: number, day: CultureDay) => any;
  onDayDelete: (index: number) => any;
};

function DayListItem(props: DayItemProps) {
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [dayPickerVisible, setDayPickerVisible] = useState(false);
  const [dayIndex, setDayIndex] = useState(props.day.dayIndex);
  const [dayIndexText, setDayIndexText] = useState(
    props.day.dayIndex.toString(),
  );

  const _onPressTime = () => {
    setIsDateTimePickerVisible(true);
  };

  const _drawTimePicker = () => (
    <DateTimePicker
      mode={'time'}
      isVisible={isDateTimePickerVisible}
      onConfirm={_onConfirmTime}
      onCancel={_onCancelTime}
    />
  );

  const _onConfirmTime = (date: Date) => {
    setIsDateTimePickerVisible(false);

    let param = new CultureParameter();
    param.id = uuid.v1();
    param.startTime = CultureTime.createNew(date.getHours(), date.getMinutes());

    const day = props.day;
    day.parametersConfigurationEntries.push(param);
    props.onDayUpdate(props.index, day);
  };

  const _onCancelTime = (date: Date) => {
    setIsDateTimePickerVisible(false);
  };
  //endregion

  const _onParameterUpdate = (index: number, parameter: CultureParameter) => {
    console.log(
      'DayListItem _onParameterUpdate index=' +
        index +
        ', parameter=' +
        JSON.stringify(parameter),
    );
    const {day} = props;
    day.parametersConfigurationEntries[index] = parameter;
    props.onDayUpdate(props.index, day);
  };

  const _onParameterDelete = (index: number) => {
    const {day} = props;
    day.parametersConfigurationEntries.splice(index, 1);
    props.onDayUpdate(props.index, day);
  };

  //region DayIndex
  const _onPressDay = () => {
    console.log('DayListItem::onPressDay');
    setDayPickerVisible(true);
    setDayIndex(props.day.dayIndex);
  };

  const _drawDayIndexPopup = () => (
    <Dialog
      rounded={true}
      visible={dayPickerVisible}
      dialogTitle={
        <DialogTitle
          title="Change day index"
          textStyle={{...styleFontStyle()}}
        />
      }
      onHardwareBackPress={() => {
        setDayPickerVisible(false);
        return true;
      }}
      onTouchOutside={() => {
        setDayPickerVisible(false);
      }}>
      <DialogContent>
        <SimpleInput
          style={{marginTop: AppTheme.screen.padding, width: scale(120)}}
          autoFocus={true}
          keyboardType={'number-pad'}
          value={`${dayIndexText}`}
          onChangeText={_onInputDayChanged}
        />

        <CoreButton
          disabled={dayIndexText.length === 0}
          style={{marginTop: AppTheme.screen.padding}}
          title={'Change day'}
          onPress={_doChangeDay}
        />
      </DialogContent>
    </Dialog>
  );

  const _onInputDayChanged = (text: string) => {
    console.log('CultureDayListView::_onInputDayChanged text=' + text);
    //this._validateDayInput(text);
    setDayIndexText(text);
  };

  const _doChangeDay = () => {
    setDayPickerVisible(false);

    let day = props.day;
    day.dayIndex = parseInt(dayIndexText);

    props.onDayUpdate(props.index, day);
  };

  const _validateDayInput = (e) => {
    setDayIndexText(e);
  };

  const _onPressDelete = () => {
    AlertUtils.yesNo(
      `Remove Culture Day ${props.day.dayIndex}`,
      'Are you sure you want this culture day ?',
      () => {
        props.onDayDelete(props.index);
      },
    );
  };

  let entries =
    props.day && props.day.parametersConfigurationEntries
      ? props.day.parametersConfigurationEntries
      : [];
  return (
    <View>
      <View style={dayStyles.container}>
        <View style={dayStyles.titleContainer}>
          <TouchableOpacity style={{flex: 1}} onPress={_onPressDay}>
            <FuturaText
              text={`Day ${props.day.dayIndex}`}
              style={dayStyles.title}
            />
          </TouchableOpacity>
          <CoreIconButton
            iconName={'remove-circle'}
            overrideIcon={'material'}
            color={AppTheme.alertColor}
            size={Dimen.iconSize.small}
            onPress={_onPressDelete}
          />
        </View>
        {entries.map((value, index) => (
          <ParameterListItem
            parameter={value}
            index={index}
            key={value.id || index.toString()}
            onParameterDelete={_onParameterDelete}
            onParameterUpdate={_onParameterUpdate}
          />
        ))}

        <SecondaryButton
          textColor={Colors.darkGrey}
          title={'Add time'}
          textStyle={styles.addTimeText}
          iconName={'plus'}
          buttonStyle={styles.addTimeButton}
          onPress={_onPressTime}
        />

        {_drawTimePicker()}

        {_drawDayIndexPopup()}
      </View>
      <View
        style={{
          marginTop: AppTheme.screen.padding,
          height: 1,
          backgroundColor: Colors.grey,
        }}
      />
    </View>
  );
}

const dayStyles = ScaledSheet.create({
  container: {
    paddingTop: AppTheme.screen.padding,
  },

  titleContainer: {
    marginBottom: AppTheme.screen.padding,
    marginLeft: AppTheme.screen.padding,
    marginRight: AppTheme.screen.padding,
    flexDirection: 'row',
    alignItems: 'center',
  },

  title: {
    color: AppTheme.primaryColor,
  },
});

//endregion

type ListProps = {
  observations?: string;
  showObservations?: boolean;
  attachments?: Attachment[];
  days: CultureDay[];
  minTemperature: number | undefined | null;
  maxTemperature: number | undefined | null;
  updatedAt?: number;
  onDaysUpdate: (
    days: CultureDay[],
    minTemperature: number | undefined | null,
    maxTemperature: number | undefined | null,
    observations?: string,
  ) => any;
};

export default function CultureDayListView(props: ListProps) {
  const [addDayModalVisible, setAddDayModalVisible] = useState(false);
  const [dayIndexText, setDayIndexText] = useState('');
  const [downloading, setDownloading] = useState(false);
  const [downloaded, setDownloaded] = useState(false);

  const _renderItem = ({item, index}) => {
    return (
      <DayListItem
        day={item}
        index={index}
        key={index.toString()}
        onDayUpdate={_onDayUpdate}
        onDayDelete={_onDayDelete}
      />
    );
  };

  const _onDayUpdate = (index: number, day: CultureDay) => {
    console.log(
      'CultureDayListView _onDayUpdate index=' +
        index +
        ', day=' +
        JSON.stringify(day),
    );
    let days = props.days;
    days[index] = day;
    props.onDaysUpdate(
      days,
      props.minTemperature,
      props.maxTemperature,
      props.observations,
    );
  };

  const _onDayDelete = (index: number) => {
    let days = props.days;
    days.splice(index, 1);
    props.onDaysUpdate(
      days,
      props.minTemperature,
      props.maxTemperature,
      props.observations,
    );
  };

  const _onDayAdd = () => {
    setAddDayModalVisible(true);
  };

  const _onInputDayChanged = (text: string) => {
    setDayIndexText(text);
  };

  const _doAddDay = () => {
    setAddDayModalVisible(false);

    let day = new CultureDay();
    day.id = uuid.v1();
    day.dayIndex = parseInt(dayIndexText);

    const days = props.days.slice();
    days.push(day);

    props.onDaysUpdate(
      days,
      props.minTemperature,
      props.maxTemperature,
      props.observations,
    );
  };

  const _onGeneralSettingsUpdate = (
    minTemperature?: number | null,
    maxTemperature?: number | null,
    observations?: string,
  ) => {
    props.onDaysUpdate(
      props.days,
      minTemperature,
      maxTemperature,
      observations,
    );
  };

  const _validateDayInput = (e) => {
    return notZero(e) && numbersOnly(e)
      ? setDayIndexText(parseInt(e).toString())
      : false;
  };

  /*const _downloadAttachments = () => {
    PermissionUtils.checkWithAlertDownloads().then((value) => {
      if (value !== 'granted') {
        return;
      }
      if (!props.attachments) {
        return;
      }
      let promises = [];
      for (let a of props.attachments) {
        if (!a || !a.url) {
          return;
        }

        let path = RNFetchBlob.fs.dirs.DownloadDir + '/' + a.filename;

        console.log('Downloading ' + a.url + ' to ' + path);
        promises.push(
          RNFetchBlob.config({
            addAndroidDownloads: {
              useDownloadManager: true, // <-- this is the only thing required
              // Optional, override notification setting (default to true)
              //notification: false,
              // Optional, but recommended since android DownloadManager will fail when
              // the url does not contains a file extension, by default the mime type will be text/plain
              mime: 'text/plain',
              description: 'File downloaded by download manager.',
              path: path,
            },
            fileCache: true,
          }).fetch('GET', a.url),
        );
      }
      setDownloading(true);
      Promise.all(promises)
        .then((value) => {
          setDownloading(false);
          setDownloaded(true);
        })
        .catch((reason) => {
          setDownloading(false);
          setDownloaded(false);
        });
    });
  };*/

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.list}
        data={props.days}
        renderItem={_renderItem}
        keyExtractor={(item, index) =>
          item && item.id
            ? item.id.toString() + '_' + index.toString()
            : index.toString()
        }
        ListHeaderComponent={
          <View>
            {props.updatedAt && (
              <FuturaText
                text={`Last updated at: ${moment(props.updatedAt).format(
                  'HH:mm DD/MM/yyyy',
                )}`}
                style={styles.updateDate}
              />
            )}
            {props.attachments && props.attachments.length > 0 && (
              <>
                {/* <View style={styles.downloadContainer}>
                {downloading ? (
                  <ActivityIndicator color={Colors.teal} />
                ) : downloaded ? (
                  <FuturaText
                    text={'Attachments downloaded to Downloads folder'}
                  />
                ) : (
                  <CoreButton
                    title={`Download ${props.attachments.length} attachments`}
                    style={styles.downloadButton}
                    onPress={_downloadAttachments}
                  />
                )}
              </View>*/}
                <View style={styles.screenMargin}>
                  <AppTitleText
                    text={'Attachments'}
                    style={{marginTop: scale(6)}}
                  />
                  {props.attachments.map((a, index) => (
                    <AttachmentListItem attachment={a} key={index.toString()} />
                  ))}
                </View>
              </>
            )}
            <GeneralSettingsView
              showsObservations={props.showObservations}
              observations={props.observations}
              minTemperature={props.minTemperature}
              maxTemperature={props.maxTemperature}
              onUpdate={_onGeneralSettingsUpdate}
            />
          </View>
        }
        ListFooterComponent={
          <CoreButton
            style={styles.addDayButton}
            title={'Add culture day'}
            onPress={_onDayAdd}
          />
        }
      />

      <Dialog
        rounderd={true}
        visible={addDayModalVisible}
        dialogTitle={
          <DialogTitle
            title="Insert day index"
            textStyle={{...styleFontStyle()}}
          />
        }
        onHardwareBackPress={() => {
          setAddDayModalVisible(false);
          return true;
        }}
        onTouchOutside={() => {
          setAddDayModalVisible(false);
        }}>
        <DialogContent>
          <SimpleInput
            style={{marginTop: AppTheme.screen.padding, width: scale(120)}}
            autoFocus={true}
            keyboardType={'number-pad'}
            value={dayIndexText}
            onChangeText={_onInputDayChanged}
          />

          <CoreButton
            disabled={dayIndexText.length === 0}
            style={{marginTop: AppTheme.screen.padding}}
            title={'Add day'}
            onPress={_doAddDay}
          />
        </DialogContent>
      </Dialog>
    </View>
  );
}

const numbersOnly = (e: string) => {
  return /^\d+$/.test(e.toString());
};

const notZero = (e: string) => {
  return !/0/.test(parseInt(e).toString());
};

type AttachmentProps = {
  attachment: Attachment;
};

function AttachmentListItem({attachment}: AttachmentProps) {
  return (
    <SecondaryButton
      textColor={Colors.blue}
      textStyle={{fontSize: scale(14)}}
      buttonStyle={{alignSelf: 'flex-start', marginTop: scale(6)}}
      title={attachment.filename}
      onPress={() => {
        if (attachment.url != null) {
          Linking.openURL(attachment.url);
        }
      }}
    />
  );
}

const styles = ScaledSheet.create({
  container: {flex: 1},

  list: {flex: 1},

  downloadContainer: {
    marginHorizontal: '20@s',
    marginVertical: '8@s',
    height: '40@s',
    justifyContent: 'center',
    alignItems: 'center',
  },

  downloadButton: {
    width: '100%',
  },

  addDayButton: {
    margin: AppTheme.screen.padding,
    width: '180@s',
    alignSelf: 'center',
  },

  addTimeText: {
    textDecorationLine: 'underline',
  },

  addTimeButton: {
    alignSelf: 'flex-end',
    marginRight: AppTheme.screen.padding,
  },

  addDayModal: {
    backgroundColor: AppTheme.backgroundColor,
    width: '300@s',
    height: '200@s',
    justifyContent: 'center',
    alignItems: 'center',
  },

  dayListItem: {},

  parameterContent: {
    backgroundColor: AppTheme.backgroundColor,
    paddingRight: AppTheme.screen.padding,
    paddingLeft: AppTheme.screen.padding,
  },

  parameterOptionContent: {
    marginLeft: '5@s',
    marginRight: '5@s',
  },

  updateDate: {
    marginHorizontal: AppTheme.screen.padding,
    fontSize: '14@s',
  },

  screenMargin: {
    marginHorizontal: AppTheme.screen.padding,
  },
});

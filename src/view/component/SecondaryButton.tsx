import React, {Component, useCallback, useState} from 'react';
import {
  View,
  TouchableOpacity,
  StyleProp,
  ViewStyle,
  TextStyle,
} from 'react-native';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {Colors} from '../../style/Colors';
import FuturaText, {FontSize, FontStyle, styleFontSize} from './FuturaText';
import type {PressEvent} from 'react-native/Libraries/Types/CoreEventTypes';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Dimen} from '../../style/Dimen';

type Props = {
  buttonStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  textFontStyle?: FontStyle;
  title: string;
  onPress?: () => any;
  textColor?: string;
  pressTextColor?: string;
  iconName?: string;
  disabled?: boolean;
};

export default function SecondaryButton(props: Props) {
  const [textColor, setTextColor] = useState(props.textColor ?? Colors.white);

  const _onPressIn = useCallback(
    (event: PressEvent) => {
      setTextColor(props.pressTextColor ?? Colors.teal);
    },
    [props],
  );

  const _onPressOut = useCallback(
    (event: PressEvent) => {
      setTextColor(props.textColor ?? Colors.white);
    },
    [props],
  );

  return (
    <TouchableOpacity
      style={[styles.button, props.buttonStyle]}
      onPress={props.onPress}
      onPressIn={_onPressIn}
      disabled={props.disabled}
      onPressOut={_onPressOut}>
      {props.iconName && (
        <Icon
          name={props.iconName}
          size={Dimen.iconSize.micro}
          color={textColor}
        />
      )}
      {props.iconName && <View style={{width: scale(4)}} />}

      <FuturaText
        text={props.title}
        fontSize={FontSize.MICRO}
        fontStyle={props.textFontStyle}
        style={[styles.title, props.textStyle, {color: textColor}]}
      />
    </TouchableOpacity>
  );
}

const styles = ScaledSheet.create({
  button: {
    //width: '100%', height: '30@s',
    //backgroundColor: '#ff8fe3',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  title: {
    //       flex: 1,
    ...styleFontSize(FontSize.SMALL),
  },
});

/*
export default class SecondaryButton extends Component<{
    buttonStyle?: StyleSheet,
    textStyle?: StyleSheet,
    textFontStyle?: FontStyle,
    title: String,
    onPress?: () => any,
    textColor?: number,
    pressTextColor?: number,
    iconName?: string,
}> {
    constructor(props) {
        super(props);

        this.state = {
            textColor: this.props.textColor ?? Colors.white,
        };
    }

    render() {
        return (<TouchableOpacity
            style={[styles.button, this.props.buttonStyle]} onPress={this.props.onPress}
            onPressIn={this._onPressIn} onPressOut={this._onPressOut}>
            {this.props.iconName && <Icon
                name={this.props.iconName} size={Dimen.iconSize.micro} color={this.state.textColor}/>}
            {this.props.iconName && <View style={{width: scale(4)}}/>}

            <FuturaText
                text={this.props.title} fontSize={FontSize.MICRO} fontStyle={this.props.textFontStyle}
                style={[styles.title, this.props.textStyle, {color: this.state.textColor}]}/>
        </TouchableOpacity>);
    }

    _onPressIn = (event: PressEvent) => {
        this.setState({
            textColor: this.props.pressTextColor ?? Colors.teal,
        });
    };

    _onPressOut = (event: PressEvent) => {
        this.setState({
            textColor: this.props.textColor ?? Colors.white,
        });
    };
}

const styles = ScaledSheet.create({
    button: {
        //width: '100%', height: '30@s',
        //backgroundColor: '#ff8fe3',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    title: {
        //       flex: 1,
        ...FuturaText.styleFontSize(FontSize.SMALL),
    }
});*/

import React, {useRef} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Alert,
  Platform,
  ViewStyle,
  StyleSheet,
  StyleProp,
  ImageSourcePropType,
  ImageURISource,
} from 'react-native';
import {ScaledSheet, scale} from 'react-native-size-matters';
import {Colors} from '../../style/Colors';
import ImagePicker from 'react-native-image-crop-picker';
import {PermissionUtils} from '../../utils/PermissionUtils';
import {Assets} from '../../style/Assets';
import RNPopoverMenu from 'react-native-popover-menu';

type Props = {
  imagePath?: string;
  containerStyle?: StyleProp<ViewStyle>;
  width?: number;
  height?: number;
  displayHeight?: number;
  onImageSelect?: (imagePath?: string) => any;
};

export default function AppImagePicker(props: Props) {
  const _imageRef = useRef();

  const _sizeStyle = () => {
    return {
      width: sizeWidth(),
      height: props.displayHeight ?? sizeHeight(),
    };
  };

  const sizeWidth = () => {
    return props.width ?? scale(100);
  };

  const sizeHeight = () => {
    return props.height ?? scale(100);
  };

  const _onPressPhotos = () => {
    PermissionUtils.checkWithAlertPhotos()
      .then((status) => {
        if (status !== 'granted') {
          Alert.alert(
            'Permission denied',
            "We don't have permission to access your photos",
          );
        } else {
          ImagePicker.openPicker({
            width: sizeWidth(),
            height: sizeHeight(),
            cropping: true,
          })
            .then((image) => {
              console.log(
                `CreateReviewScreen::onPressPick image=${JSON.stringify(
                  image,
                )}`,
              );
              if (props.onImageSelect) {
                props.onImageSelect(image ? image.path : undefined);
              }
            })
            .catch((reason) => {
              Alert.alert('Error', reason);
            });
        }
      })
      .catch((reason) => {
        Alert.alert('Error', reason);
      });
  };

  const _onPressCamera = () => {
    PermissionUtils.checkWithAlertCamera()
      .then((status) => {
        if (status !== 'granted') {
          Alert.alert(
            'Permission denied',
            "We don't have permission to access your photos",
          );
        } else {
          ImagePicker.openCamera({
            width: sizeWidth(),
            height: sizeHeight(),
            cropping: true,
          })
            .then((image) => {
              console.log(
                `CreateReviewScreen::onPressPick image=${JSON.stringify(
                  image,
                )}`,
              );
              if (props.onImageSelect) {
                props.onImageSelect(image ? image.path : undefined);
              }
            })
            .catch((reason) => {
              Alert.alert('Error', reason);
            });
        }
      })
      .catch((reason) => {
        Alert.alert('Error', reason);
      });
  };

  const _onPressClearPhoto = () => {
    if (props.onImageSelect) {
      props.onImageSelect(undefined);
    }
  };

  const _onPress = () => {
    let menus = [
      {
        menus: [
          {
            label: 'Pick from Gallery',
            // icon: this._getMenuIcon(this.state.pot.schedule ? 'edit' : 'add')
          },
          {
            label: 'Take Photo',
          },
        ],
      },
    ];

    if (props.imagePath) {
      menus[0].menus.push({
        label: 'Clear Photo',
      });
    }

    RNPopoverMenu.Show(_imageRef.current, {
      title: '',
      menus: menus,
      onDone: (index, menuIndex) => {
        console.log(
          'AppImagePicker::Menu onDone index=' +
            index +
            ', menuIndex=' +
            menuIndex,
        );
        if (
          (Platform.OS === 'ios' && index === 0) ||
          (Platform.OS === 'android' && menuIndex === 0)
        ) {
          _onPressPhotos();
        } else if (
          (Platform.OS === 'ios' && index === 1) ||
          (Platform.OS === 'android' && menuIndex === 1)
        ) {
          //take photo
          _onPressCamera();
        } else if (
          (Platform.OS === 'ios' && index === 2) ||
          (Platform.OS === 'android' && menuIndex === 2)
        ) {
          //clear image
          _onPressClearPhoto();
        }
      },
      onCancel: () => {},
      //ios only,
      menuWidth: scale(200),
      borderColor: Colors.grey,
      borderWidth: scale(1),
      separatorColor: Colors.grey,
    });
  };

  let imageSource: ImageURISource | undefined = props.imagePath
    ? {uri: props.imagePath}
    : undefined;
  return (
    <TouchableOpacity
      style={[imageHolderStyles.container, _sizeStyle(), props.containerStyle]}
      onPress={_onPress}>
      <Image
        ref={_imageRef}
        style={[imageHolderStyles.image, _sizeStyle()]}
        source={imageSource}
      />

      {!imageSource && (
        <View style={imageHolderStyles.placeholderContainer}>
          <Image
            style={imageHolderStyles.placeholder}
            source={Assets.icon.template}
          />
        </View>
      )}
    </TouchableOpacity>
  );
}

const imageHolderStyles = ScaledSheet.create({
  container: {},
  image: {
    resizeMode: 'cover',
    borderWidth: 1,
    borderColor: Colors.grey,
    borderRadius: '10@s',
  },

  placeholderContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },

  placeholder: {
    width: '60@s',
    height: '60@s',
    resizeMode: 'contain',
  },
});

import React from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import {RaisedTextButton} from 'react-native-material-buttons';
import {AppTheme} from '../../style/AppTheme';
import {Colors} from '../../style/Colors';
import {ScaledSheet} from 'react-native-size-matters';
import {FontSize, FontStyle, styleFont} from './FuturaText';
import {Button} from 'react-native-elements';

type Props = {
  style?: StyleProp<ViewStyle>;
  title: string;
  onPress?: () => any;
  disabled?: boolean;
};

export default function CoreButton({style, title, onPress, disabled}: Props) {
  return (
    <Button
      raised
      disabled={disabled}
      /*style={[styles.button, style]}*/
      /*rippleContainerBorderRadius={AppTheme.button.borderRadius}*/
      /*shadeBorderRadius={AppTheme.button.borderRadius}*/
      containerStyle={[styles.container, style]}
      titleStyle={styles.title}
      title={title}
      buttonStyle={[styles.button]}
      onPress={onPress}
    />
  );
}

const styles = ScaledSheet.create({
  button: {
    height: AppTheme.button.height,
    borderRadius: AppTheme.button.borderRadius,
    backgroundColor: AppTheme.primaryColor,
  },

  container: {
    borderRadius: AppTheme.button.borderRadius,
  },

  title: {
    ...styleFont(FontStyle.BOOK, FontSize.MEDIUM),
    color: Colors.white,
  },
});


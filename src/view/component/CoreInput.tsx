import React, {
  forwardRef,
  Ref,
  useCallback,
  useImperativeHandle,
  useRef,
} from 'react';
import {
  Image,
  View,
  TextInput,
  StyleProp,
  ViewStyle,
  ImageSourcePropType,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ScaledSheet} from 'react-native-size-matters';
import {applySize, Dimen} from '../../style/Dimen';
import {AppTheme} from '../../style/AppTheme';
import {Colors} from '../../style/Colors';
import FuturaText, {FontStyle, styleFontStyle} from './FuturaText';
import type {
  AutoCapitalize,
  KeyboardType,
  ReturnKeyType,
} from 'react-native/Libraries/Components/TextInput/TextInput';

import {findNodeHandle} from 'react-native';
import TextInputState from 'react-native/Libraries/Components/TextInput/TextInputState';

export function focusTextInput(node: TextInput) {
  try {
    TextInputState.focusTextInput(findNodeHandle(node));
  } catch (e) {
    console.log("Couldn't focus text input: ", e.message);
  }
}

export interface CoreInputV2Type {
  focus: () => void;
}

type Props = {
  style?: StyleProp<ViewStyle>;
  id?: string;
  label: string;
  text: string;
  error: string;
  icon?: ImageSourcePropType;
  iconName?: string;
  onInputChange?: (id: string | undefined, text: string) => void;
  onInputEnd?: (id: string | undefined, text: string) => void;
  onSubmitEditing?: (id: string | undefined, text: string) => any;
  secureTextEntry?: boolean;
  keyboardType?: KeyboardType;
  returnKeyType?: ReturnKeyType;
  hasNextInput?: boolean;
  autoCapitalize?: AutoCapitalize;
  maxLength?: number;
};

const CoreInputV2 = forwardRef(
  (
    {
      style,
      id,
      label,
      text,
      error,
      icon,
      iconName,
      onInputChange,
      onInputEnd,
      onSubmitEditing,
      secureTextEntry,
      keyboardType,
      returnKeyType,
      hasNextInput,
      autoCapitalize,
      maxLength,
    }: Props,
    ref: Ref<CoreInputV2Type>,
  ) => {
    const _textInput = useRef<TextInput>(null);

    const _onChangeText = useCallback(
      (textNew) => {
        if (onInputChange) {
          onInputChange(id, textNew);
        }
      },
      [id, onInputChange],
    );

    const focus = useCallback(() => {
      _textInput.current?.focus();
    }, []);

    useImperativeHandle(ref, () => ({focus}));

    const _onBlur = useCallback(() => {
      console.log(`LoginInput::_onBlur text: '${text}'`);
      if (onInputEnd) {
        onInputEnd(id, text);
      }
    }, [id, onInputEnd, text]);

    const _onSubmitEditing = useCallback(() => {
      if (onSubmitEditing) {
        onSubmitEditing(id, text);
      }
    }, [id, onSubmitEditing, text]);

    return (
      <View style={[styles.container, style]}>
        <View style={styles.inputContainer}>
          {icon && <Image source={icon} style={styles.inputIcon} />}
          {iconName && (
            <Icon
              name={iconName}
              color={Colors.darkGrey}
              size={AppTheme.input.iconSize}
              style={styles.inputIconFontAwesome}
            />
          )}
          <TextInput
            autoCapitalize={autoCapitalize}
            maxLength={maxLength}
            ref={_textInput}
            style={styles.input}
            placeholder={label}
            underlineColorAndroid="transparent"
            secureTextEntry={secureTextEntry}
            keyboardType={keyboardType}
            onChangeText={_onChangeText}
            onBlur={_onBlur}
            autoCompleteType={'password'}
            returnKeyType={returnKeyType}
            onSubmitEditing={_onSubmitEditing}
            blurOnSubmit={!hasNextInput}
          />
        </View>
        <FuturaText text={error ?? ' '} style={styles.error} />
      </View>
    );
  },
);

export default CoreInputV2;

/*
export default class CoreInput extends Component<{
  style?: StyleSheet;
  id?: string;
  label: string;
  text: string;
  error?: string;
  icon?: ImageSource;
  iconName?: string;
  onInputChange?: (id?: string, text: string) => void;
  onInputEnd?: (id?: string, text: String) => void;
  onSubmitEditing?: (id?: string, text: string) => any;
  secureTextEntry?: Boolean;
  keyboardType?: ?KeyboardType;
  returnKeyType?: ?ReturnKeyType;
  hasNextInput?: boolean;
  autoCapitalize?: ?AutoCapitalize;
  maxLength?: number;
}> {
  constructor(props) {
    super(props);
  }

  render() {
    {
      // style={styles.inputIconFontAwesome}
    }
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.inputContainer}>
          {this.props.icon && (
            <Image source={this.props.icon} style={styles.inputIcon} />
          )}
          {this.props.iconName && (
            <Icon
              name={this.props.iconName}
              color={Colors.darkGrey}
              size={AppTheme.input.iconSize}
              style={styles.inputIconFontAwesome}
            />
          )}
          <TextInput
            autoCapitalize={this.props.autoCapitalize}
            maxLength={this.props.maxLength}
            ref={(input) => {
              this._textInput = input;
            }}
            style={styles.input}
            placeholder={this.props.label}
            underlineColorAndroid="transparent"
            secureTextEntry={this.props.secureTextEntry}
            keyboardType={this.props.keyboardType}
            onChangeText={this._onChangeText}
            onBlur={this._onBlur}
            autoCompleteType={'password'}
            returnKeyType={this.props.returnKeyType}
            onSubmitEditing={this.props.onSubmitEditing}
            blurOnSubmit={!this.props.hasNextInput}
          />
        </View>
        <FuturaText text={this.props.error ?? ' '} style={styles.error} />
      </View>
    );
  }

  focus() {
    focusTextInput(this._textInput);
    //this._textInput.focus();
  }

  /* _getKeyboardType() {
         switch (this.props.type) {
             case InputTypes.email: return 'email-address';
         }
         return 'default';
 //    }*/

/*_getAutoCapitalize() {
        switch (this.props.type) {
            case InputTypes.password:
            case InputTypes.confirmPassword: return 'none';
        }
        return 'sentences';
//    }

  _onChangeText = (text) => {
    if (this.props.onInputEnd) {
      this.props.onInputChange(this.props.id, text);
    }
  };

  _onBlur = () => {
    console.log(`LoginInput::_onBlur text: '${this.props.text}'`);

    if (this.props.onInputEnd) {
      this.props.onInputEnd(this.props.id, this.props.text);
    }
  };

  _onSubmitEdditing = () => {
    if (this.props.onSubmitEditing) {
      this.props.onSubmitEditing(this.props.id, this.props.text);
    }
  };
}*/

const styles = ScaledSheet.create({
  container: {
    width: '100%',
  },

  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //marginBottom: '-20@s',
    width: '100%',
    backgroundColor: Colors.white,
    height: AppTheme.input.height,
    borderRadius: AppTheme.input.borderRadius,
    paddingLeft: '26@s',
    paddingRight: '26@s',
  },
  inputIcon: {
    ...applySize(Dimen.iconSize.small),
    resizeMode: 'contain',
    marginRight: '16@ms',
  },
  inputIconFontAwesome: {
    marginRight: '16@ms',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: AppTheme.input.iconSize,
  },
  inputMaterial: {
    flex: 1,
    height: '30@s',
    marginBottom: '27@s',
    alignSelf: 'flex-end',
  },

  inputMaterialTitle: {
    marginBottom: '40@s',
  },

  input: {
    ...styleFontStyle(FontStyle.BOOK),
    flex: 1,
    //backgroundColor: '#fea8ff',
  },

  error: {
    ...styleFontStyle(FontStyle.BOOK),
    marginLeft: '26@s',
    color: Colors.red,
    fontSize: '8@s',
    height: '10@s',
  },
});

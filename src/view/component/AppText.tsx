import FuturaText from './FuturaText';
import React from 'react';
import {ScaledSheet} from 'react-native-size-matters';
import {AppTheme} from '../../style/AppTheme';
import type {FuturaTextProps} from './FuturaText';

export function AppTitleText({style, ...textProps}: FuturaTextProps) {
  return <FuturaText style={[styles.title, style]} {...textProps} />;
}

export function AppLabelText(props: FuturaTextProps) {
  return <FuturaText {...props} />;
}

const styles = ScaledSheet.create({
  title: {
    color: AppTheme.primaryColor,
  },

  label: {},
});

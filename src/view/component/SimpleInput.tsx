import React from 'react';
import {StyleProp, TextInput, TextInputProps, ViewStyle} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import FuturaText, {
  FontSize,
  FontStyle,
  styleFontStyle,
  styleFontSize,
} from './FuturaText';
import {AppTheme} from '../../style/AppTheme';

type Props = TextInputProps & {
  style?: StyleProp<ViewStyle>;
};

export default function SimpleInput({style, ...props}: Props) {
  return <TextInput {...props} style={[styles.input, style]} />;
}
const styles = ScaledSheet.create({
  input: {
    ...styleFontStyle(FontStyle.BOOK),
    ...styleFontSize(FontSize.MEDIUM),
    width: '100%',
    borderBottomColor: AppTheme.primaryColor,
    borderBottomWidth: '1@s',
  },
});

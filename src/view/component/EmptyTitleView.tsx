import {View} from 'react-native';
import FuturaText from './FuturaText';
import {AppTheme} from '../../style/AppTheme';
import React from 'react';

type Props = {
  title: string;
};

export default function EmptyTitleView({title}: Props) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <FuturaText
        style={{textAlign: 'center', color: AppTheme.primaryColor}}
        text={title}
      />
    </View>
  );
}

import database, {FirebaseDatabaseTypes} from '@react-native-firebase/database';
import LoginManager from './LoginManager';
import UserPot from '../model/UserPot';
import _ from 'lodash';
import PhotoRequest from '../model/PhotoRequest';

class PotManager {
  static _initialize(): PotManager {
    return new PotManager();
  }

  updateRelayState(
    pot: UserPot,
    stateType: string,
    value: number,
  ): Promise<any> {
    let o: any = {};
    o[stateType] = value;
    let path = `/users/${LoginManager.userId}/pots/${pot.id}/relayState`;
    console.log(
      "PotManager::updateRelayState path='" +
        path +
        "' relayState=" +
        JSON.stringify(o),
    );
    return database()
      .ref(`/users/${LoginManager.userId}/pots/${pot.id}/relayState`)
      .update(o);
  }

  requestPhoto(pot: UserPot): Promise<any> {
    return database()
      .ref(`/users/${LoginManager.userId}/pots/${pot.id}/photoRequest`)
      .update(PhotoRequest.init());
  }

  updateName(pot: UserPot, name: string): Promise<any> {
    return database()
      .ref(`/users/${LoginManager.userId}/pots/${pot.id}`)
      .update({name});
  }

  getPot(potId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const path = `/users/${LoginManager.userId}/pots/${potId}`;
      console.log('PotManager::getPot path=' + path);
      database()
        .ref(path)
        .once('value')
        .then((snapshot) => {
          const val = snapshot.val();
          console.log('PotManager::getPot val=' + val);
          if (!val) {
            reject('Pot not found for this user');
            return;
          }
          const pot = new UserPot(val);
          resolve(pot);
        })
        .catch((reason) => {
          console.log('PotManager::getPot reason' + JSON.stringify(reason));
          reject(reason.message);
        });
    });
  }

  subscribeAllPots(
    callbackFn: (pots: UserPot[]) => any,
  ): FirebaseDatabaseTypes.Reference {
    const path = `/users/${LoginManager.userId}/pots`;
    console.log('PotManager::subscribeAllPots path=' + path);
    let ref = database().ref(path);
    ref.on('value', (snapshot) => {
      const val = snapshot.val();
      console.log('PotManager::subscribeAllPots');// val=' + JSON.stringify(val));
      const pots: UserPot[] = [];
      if (val) {
        _.toArray(val).forEach((value) => {
          let pot = new UserPot(value);
          if (pot.id) {
            pots.push(pot);
          }
        });
      }
      console.log(
        'PotManager::subscribeAllPots received ' + pots.length + ' pots',
      );
      //const pots = val ? _.toArray(val).map(value => new UserPot(value)) : [];
      callbackFn(pots);
    });
    return ref;
  }

  subscribePot(potId: number, callbackFn: (pot: UserPot) => any): FirebaseDatabaseTypes.Reference {
    console.log('PotManager::subscribePot subscribing to pot ' + potId);
    let ref = database()
      .ref(`/users/${LoginManager.userId}/pots/${potId}`);
    ref.on('value', (snapshot) => {
      const val = snapshot.val();

      if (!val) {
        return;
      }
      const pot = new UserPot(val);
      console.log(
        'UserManager::subscribePot pot.photoRequest=' +
          JSON.stringify(pot.photoRequest) +
          ', from val ' +
          JSON.stringify(val.photoRequest),
      );
      callbackFn(pot);
    });
    return ref;
  }

  unsubscribe(ref: FirebaseDatabaseTypes.Reference) {
    console.log('PotManager::unsubscribePot unsubscribing ref');
    if (!ref) {
      return;
    }
    ref.off();
  }

  savePotSchedule(pot: UserPot): Promise<any> {
    console.log(
      'PotManager::savePotSchedule schedule: ' + JSON.stringify(pot.schedule),
    );
    return new Promise((resolve, reject) => {
      database()
        .ref(`/users/${LoginManager.userId}/pots/${pot.id}/schedule`)
        .set(pot.schedule)
        .then((potSnapshot) => {
          resolve();
        })
        .catch((reason: any) => {
          reject(reason.message);
        });
    });
  }

  removePot(potId: string | undefined): Promise<any> {
    return new Promise((resolve, reject) => {
      database()
        .ref(`/pots/${potId}`)
        .update({
          used: false,
          userId: '',
        })
        .then((snapshot) => {
          database()
            .ref(`/users/${LoginManager.userId}/pots/${potId}`)
            .remove()
            .then((deleteSnapshot) => {
              resolve();
            })
            .catch((reason: any) => {
              reject(reason.message);
            });
        })
        .catch((reason: any) => {
          reject(reason.message);
        });
    });
  }
}

export default PotManager = PotManager._initialize();

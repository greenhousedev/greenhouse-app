import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';
import UserManager from './UserManager';

class NotificationsManagerClass {
  static _initialize(): NotificationsManagerClass {
    return new NotificationsManagerClass();
  }

  _notificationListener?: () => void;
  _notificationOpenedListener?: () => void;

  start() {
    console.log('NotificationsManager::start');
    this._checkPermission();
    //this._createNotificationListeners(); //add this line
  }

  stop() {
    if (this._notificationListener) {
      this._notificationListener();
    }
    if (this._notificationOpenedListener) {
      this._notificationOpenedListener();
    }
  }

  //1
  async _checkPermission() {
    const enabled = await messaging().hasPermission();
    console.log('NotificationsManager::_checkPermission enabled=' + enabled);
    if (enabled) {
      this._getToken();
    } else {
      this._requestPermission();
    }
  }

  //3
  async _getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        console.log('NotificationsManager::getToken ' + fcmToken);
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    } else {
      console.log('NotificationsManager::getToken ' + fcmToken);
    }
    if (fcmToken) {
      UserManager.addFcmToken(fcmToken);
    }
  }

  //2
  async _requestPermission() {
    try {
      await messaging().requestPermission();
      // User has authorised
      this._getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async _createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this._notificationListener = messaging().onMessage((notification) => {
      const {title, body} = notification;
      this._showAlert(title, body);
    });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this._notificationOpenedListener = messaging().onNotificationOpenedApp(
      (notificationOpen) => {
        const {title, body} = notificationOpen.notification;
        this._showAlert(title, body);
      },
    );

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const {title, body} = notificationOpen.notification;
      this._showAlert(title, body);
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }

  _showAlert(title, body) {
    /*Alert.alert(
      title, body,
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );*/
  }
}

const NotificationsManager = NotificationsManagerClass._initialize();
export default NotificationsManager;

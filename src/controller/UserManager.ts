import User from '../model/User';
import LoginManager from './LoginManager';
import {Pot} from '../model/Pot';
import CultureTemplate from '../model/CultureTemplate';
import database from '@react-native-firebase/database';

let confirmed: boolean = false;

class UserManagerClass {
  static _initialize(): UserManagerClass {
    return new UserManagerClass();
  }

  user?: User;

  get confirmed(): boolean {
    return this.user?.confirmed ?? false;
  }

  confirmLink?: string;

  confirmUser(): Promise<any> {
    console.log('UserManager::confirmLink url=' + this.confirmLink);

    return new Promise((resolve, reject) => {
      database()
        .ref(`/users/${LoginManager.userId}`)
        .update({
          confirmed: true,
        })
        .then(async (value) => {
          this.confirmLink = undefined;
          await this.getUser();
          resolve();
        })
        .catch(reject);
    });
  }

  getUser(): Promise<User> {
    return new Promise((resolve, reject) => {
      if (!LoginManager.isLoggedIn) {
        reject('You are not logged in!');
        return;
      }
      //LoginManager.userId
      database()
        .ref(`/users/${LoginManager.userId}`)
        .once('value')
        .then((snapshot) => {
          const val = snapshot.val();
          console.log('UserManager::getUser val=' + JSON.stringify(val));
          if (!val) {
            const userEmail = LoginManager.userEmail;
            if (!userEmail) {
              reject('invalid email');
              return;
            }
            const user = User.initUser(userEmail);
            console.log(
              'UserManager::getUser create first time login user=' +
                JSON.stringify(user),
            );
            this.createUser(user);
            this.user = user;
            resolve(user);
            return;
          }

          const user = new User(val);
          console.log('UserManager::getUser user.confirmed=' + user.confirmed);
          this.user = user;
          resolve(user);
        })
        .catch((reason) => {
          console.log('UserManager::getUser reason' + JSON.stringify(reason));
          reject(reason.message);
        });
    });
  }

  logout() {
    this.user = undefined;
  }

  createUser(user: User) {
    database().ref(`/users/${LoginManager.userId}`).set(user);
  }

  addPotToUser(potId: string, secret: string, name: string): Promise<any> {
    return new Promise((resolve, reject) => {
      database()
        .ref(`/pots/${potId}`)
        .once('value')
        .then((snapshot) => {
          const val = snapshot.val();
          if (val) {
            const pot = new Pot(val);
            if (secret !== pot.secret) {
              reject('Failed matching secrets');
              return;
            }
            if (pot.used) {
              if (pot.userId === LoginManager.userId) {
                reject('Pot is already in added in your account');
              } else {
                reject('Pot is used by other account');
              }
              return;
            }
            database()
              .ref(`/pots/${potId}`)
              .update({
                used: true,
                userId: LoginManager.userId,
              })
              .then((potUpdateSnapshot) => {
                database()
                  .ref(`/users/${LoginManager.userId}/pots/${potId}`)
                  .set({
                    id: potId,
                    name,
                  })
                  .then((addPotSnapshot) => {
                    resolve();
                  })
                  .catch((reason) => {
                    reject(reason.message);
                  });
              })
              .catch((reason) => {
                reject(reason.message);
              });
          } else {
            reject(`Pot with id ${potId} not found!`);
          }
        })
        .catch((reason) => {
          reject(reason.message);
        });
    });
  }

  addTemplate(template: CultureTemplate): Promise<any> {
    template.createdAt = new Date().getTime();
    template.updatedAt = new Date().getTime();
    return new Promise((resolve, reject) => {
      database()
        .ref(`/users/${LoginManager.userId}/templates/${template.id}`)
        .set(template)
        .then((snapshot) => {
          resolve();
        })
        .catch((reason) => {
          reject(reason.message);
        });
    });
  }

  saveTemplate(template: CultureTemplate): Promise<any> {
    template.updatedAt = new Date().getTime();

    return new Promise((resolve, reject) => {
      // eslint-disable-next-line eqeqeq
      if (!template || !template.id || template.id.length == 0) {
        reject('invalid template id');
        return;
      }
      database()
        .ref(`/users/${LoginManager.userId}/templates/${template.id}`)
        .set(template)
        .then((snapshot) => {
          resolve();
        })
        .catch((reason) => {
          reject(reason.message);
        });
    });
  }

  shareTemplate(template: CultureTemplate, userId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let newTemplate: CultureTemplate;
      // eslint-disable-next-line eqeqeq
      if (!template || !template.id || template.id.length == 0) {
        reject('invalid template id');
        return;
      }
      try {
        // @ts-ignore
        newTemplate = {...template};
        newTemplate.updatedAt = new Date().getTime();
      } catch (e) {
        reject(e.message);
        return;
      }
      database()
        .ref(`/users/${userId}/templates/${newTemplate.id}`)
        .set(template)
        .then((snapshot) => {
          resolve();
        })
        .catch((reason) => {
          reject(reason.message);
        });
    });
  }

  removeTemplate(template: CultureTemplate): Promise<any> {
    console.log('UserManager::removeTemplate');
    return new Promise((resolve, reject) => {
      database()
        .ref(`/users/${LoginManager.userId}/templates/${template.id}`)
        .remove()
        .then((snapshot) => {
          console.log('UserManager::removeTemplate done');
          resolve();
        })
        .catch((reason) => {
          console.log('UserManager::removeTemplate error');
          reject(reason.message);
        });
    });
  }

  addFcmToken(fcmToken: String) {
    console.log('UserManager::addFcmToken');
    this.getUser().then((user) => {
      if (user) {
        console.log(
          'UserManager::addFcmToken user found tokens=' +
            JSON.stringify(user.fcmTokens),
        );
        let alreadyAdded = false;
        for (let i = 0; i < user.fcmTokens.length; i++) {
          if (user.fcmTokens[i] === fcmToken) {
            alreadyAdded = true;
            break;
          }
        }
        if (!alreadyAdded) {
          console.log('UserManager::addFcmToken fcmToken adding');
          user.fcmTokens.push(fcmToken);
          database()
            .ref(`/users/${LoginManager.userId}/fcmTokens`)
            .set(user.fcmTokens)
            .then((value) => {
              console.log('UserManager::addFcmToken fcmToken added');
            })
            .catch((error) => {
              console.log(
                'UserManager::addFcmToken fcmToken error ' + error.message,
              );
            });
        } else {
          console.log('UserManager::addFcmToken fcmToken already added');
        }
      } else {
        console.log('UserManager::addFcmToken no user received');
      }
    });
  }

  async removeFcmToken(fcmToken: String) {
    let tokenRemoved = false;
    await this.getUser().then((user) => {
      if (user) {
        console.log(
          'UserManager::removeFcmToken user found tokens=' +
            JSON.stringify(user.fcmTokens),
        );
        let tokenFound = false;
        if (user.fcmTokens) {
          for (let i = 0; i < user.fcmTokens.length; i++) {
            if (user.fcmTokens[i] === fcmToken) {
              user.fcmTokens.splice(i, 1);
              tokenFound = true;
              break;
            }
          }
        }
        if (tokenFound) {
          console.log('UserManager::removeFcmToken fcmToken removing');
          database()
            .ref(`/users/${LoginManager.userId}/fcmTokens`)
            .set(user.fcmTokens)
            .then((value) => {
              tokenRemoved = true;
              console.log('UserManager::removeFcmToken fcmToken removed');
            })
            .catch((error) => {
              console.log(
                'UserManager::removeFcmToken fcmToken error ' + error.message,
              );
            });
        } else {
          console.log('UserManager::removeFcmToken fcmToken not found');
        }
      } else {
        console.log('UserManager::removeFcmToken no user received');
      }
    });
    return tokenRemoved;
  }

  findUserByEmail(email: string): Promise<{userId: string; user: User}> {
    console.log('UserManager::findUserByEmail email=' + email);
    return new Promise((resolve, reject) => {
      return database()
        .ref('users')
        .orderByChild('email')
        .equalTo(email)
        .once('value')
        .then((snapshot) => {
          const val = snapshot.val();
          console.log(
            'UserManager::findUserByEmail val=' + JSON.stringify(val),
          );
          if (!val) {
            reject('No user found with this email');
            return;
          }
          let keys = Object.keys(val);
          if (keys.length === 0) {
            reject('No user found with this email');
            return;
          }
          // grab only the first result for now
          const userId = keys[0];
          const data = val[userId];
          console.log(
            'UserManager::findUserByEmail userId=' +
              userId +
              ', val=' +
              JSON.stringify(data),
          );
          if (!data) {
            reject('No user found with this email');
            return;
          }
          const user = new User(data);
          resolve({userId, user});
        })
        .catch(reject);
    });
  }
}

const UserManager = UserManagerClass._initialize();
export default UserManager;

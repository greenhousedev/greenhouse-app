import AppConfig from '../model/AppConfig';
import UserPot from '../model/UserPot';
import database from '@react-native-firebase/database';

class ConfigManager {
  static _initialize(): ConfigManager {
    let c = new ConfigManager();
    c._init();
    return c;
  }

  appConfig: AppConfig | undefined;

  _init() {
    this.loadConfig();
  }

  loadConfig(callbackFn?: (config: AppConfig) => any) {
    database()
      .ref('/appConfig')
      .on('value', (snapshot) => {
        const val = snapshot.val();
        console.log('ConfigManager::appConfig val=' + JSON.stringify(val));
        if (!val) {
          return;
        }
        this.appConfig = new AppConfig(val);
        if (callbackFn) {
          callbackFn(this.appConfig);
        }
      });
  }

  get hasDebugUser(): boolean {
    return (
      this.appConfig !== undefined &&
      this.appConfig.debugUser !== undefined &&
      this.appConfig.debugUser.trim().length > 0
    );
  }
}

export default ConfigManager = ConfigManager._initialize();

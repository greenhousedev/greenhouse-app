import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
import ConfigManager from './ConfigManager';
import AsyncStorage from '@react-native-community/async-storage';
import UserManager from './UserManager';
import {Platform} from 'react-native';

class LoginManagerClass {
  static _initialize(): LoginManagerClass {
    return new LoginManagerClass();
  }

  get isLoggedIn(): boolean {
    return !!auth().currentUser;
  }

  get userId(): string | undefined {
    const loggedIn = this.isLoggedIn;
    console.log('LoginManager::userId is loggedIn=' + loggedIn);
    if (!loggedIn) {
      return undefined;
    }

    //TODO remove test

    if (ConfigManager.hasDebugUser && ConfigManager.appConfig?.debugUser) {
      return ConfigManager.appConfig?.debugUser;
    }
    return auth().currentUser?.uid;
  }

  get userEmail(): string | null | undefined {
    if (!this.isLoggedIn) {
      return undefined;
    }
    return auth().currentUser?.email;
  }

  signUp(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async (user) => {
          console.log('LoginManager::signUp user=' + JSON.stringify(user));
          await this.resendConfirmationEmail();
          resolve();
        })
        .catch((reason) => {
          console.log('LoginManager::signUp auth reason=' + reason.message);
          reject(reason.message);
        });
    });
  }

  resendConfirmationEmail(): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      try {
        const firebaseUser = auth().currentUser;
        console.log('LoginManager::resendConfirmationEmail');

        if (firebaseUser) {
          const functionsUrl = `https://us-central1-nohaegrowing.cloudfunctions.net/confirmEmail?email=${firebaseUser.email}&userId=${firebaseUser.uid}&isMobile=true`;
          const appUrl = `https://memox.page.link/confirmEmail?email=${firebaseUser.email}&userId=${firebaseUser.uid}`;

          const actionCodeSettings = {
            url: Platform.OS === 'android' ? functionsUrl : appUrl,
            handleCodeInApp: Platform.OS === 'ios',
          };
          console.log(
            'LoginManager::resendConfirmationEmail actionCodeSettings=' +
              JSON.stringify(actionCodeSettings),
          );
          try {
            await firebaseUser.sendEmailVerification(actionCodeSettings);
            resolve();
          } catch (e) {
            console.log(e);
            reject(e.message);
            return;
          }
        } else {
          reject('User not logged in');
        }
      } catch (e) {
        reject(e.message);
      }
    });
  }

  loginEmail(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      auth()
        .signInWithEmailAndPassword(email, password)
        .then((res) => {
          console.log('LoginManager::loginEmail res=' + JSON.stringify(res));
          resolve();
        })
        .catch((reason) => {
          console.log(
            'LoginManager::loginEmail auth reason=' + JSON.stringify(reason),
          );
          reject(reason.message);
        });
    });
  }

  logout(): Promise<any> {
    UserManager.logout();
    return new Promise(async (resolve, reject) => {
      let fcmToken = await AsyncStorage.getItem('fcmToken');
      if (fcmToken) {
        await UserManager.removeFcmToken(fcmToken);
      }
      auth()
        .signOut()
        .then((value) => {
          console.log('LoginManager::logout success');
          resolve();
        })
        .catch((reason) => {
          console.log(
            'LoginManager::logout auth reason=' + JSON.stringify(reason),
          );
          reject(reason.message);
        });
    });
  }

  sendResetPassword(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      auth()
        .sendPasswordResetEmail(email)
        .then((value) => {
          resolve();
        })
        .catch((reason) => {
          reject(reason.message);
        });
    });
  }
}
const LoginManager = LoginManagerClass._initialize();
export default LoginManager;

import {ScaledSheet} from 'react-native-size-matters';
import {Platform} from 'react-native';

const FuturaFontStyles = ScaledSheet.create({
  book: {
    fontFamily: Platform.OS === 'ios' ? 'FuturaPT-Book' : 'FuturaPTBook',
    fontWeight: 'normal',
  },

  medium: {
    fontFamily: Platform.OS === 'ios' ? 'FuturaPT-Medium' : 'FuturaPTMedium',
  },
});

export default FuturaFontStyles;

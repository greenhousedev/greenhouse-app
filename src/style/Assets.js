export const Assets = {
  background: {
    login: require('../assets/images/login_background.png'),
  },

  icon: {
    empty: require('../assets/images/empty.png'),

    pot: require('../assets/images/potIcon.png'),
    potShadow: require('../assets/images/potIconShadow.png'),

    fan: require('../assets/images/fanIcon.png'),
    pump: require('../assets/images/pumpIcon.png'),
    light: require('../assets/images/iconLight.png'),
    temperature: require('../assets/images/temperatureIcon.png'),
    humidity: require('../assets/images/humidityIcon.png'),

    time: require('../assets/images/timeIcon.png'),

    home: require('../assets/images/homeIcon.png'),
    schedule: require('../assets/images/scheduleIcon.png'),
    culture: require('../assets/images/cultureIcon.png'),
    template: require('../assets/images/templateIcon.png'),
    settings: require('../assets/images/settingsIcon.png'),

    ph: require('../assets/images/phIcon.png'),

    email: require('../assets/images/email.png'),
  },

  sigla: {
    s1: require('../assets/images/sigla_1.png'),
    s2: require('../assets/images/sigla_2.png'),
    s3: require('../assets/images/sigla_3.png'),
  },
};

import {scale, moderateScale} from 'react-native-size-matters';
import {Platform} from 'react-native';

export const Dimen = {
    iconSize: {
        micro: scale(10),
        veryVerySmall: scale(16),
        verySmall: scale(18),
        small: scale(24),
        medium: scale(32),
        large: scale(40),
    },

    elementHeight: scale(40),
    elementRadius: scale(20),

    font: {
        size: {
            micro: scale(8),
            verySmall: scale(10),
            small: scale(Platform.OS === "ios" ? 12 : 11),
            medium: scale(Platform.OS === "ios" ? 16 : 16),
            mediumLarge: scale(Platform.OS === "ios" ? 19 : 17),
            large: scale(Platform.OS === "ios" ? 21 : 19),
            veryLarge: scale(Platform.OS === "ios" ? 28 : 26),
            title: scale(Platform.OS === "ios" ? 40 : 38),
        },
    },
};

export const applySize = (size:number) => {
    return {
        width: size,
        height: size,
    }
};


export const Colors = {
    teal: '#0DC6B3',
    blue: '#1775db',
    white: '#FFF',
    lightGrey: '#F2F2F2',
    lightGreyTransparent: '#F2F2F288',
    grey: '#BEC1C8',
    darkGrey: '#707070',
    red: '#FF0000',
    reddish: '#FA4008',

};

import {LayoutAnimation, NativeModules} from "react-native";

const {UIManager} = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

export const Anims = {

    main: {
        duration: 500,
        create: {
            property: LayoutAnimation.Properties.opacity,
            type: LayoutAnimation.Types.spring,
            springDamping: 1,
        },
        update: {
            property: LayoutAnimation.Properties.opacity,
            type: LayoutAnimation.Types.spring,
            springDamping: 0.4,
        },
        delete: {
            duration: 50,
            property: LayoutAnimation.Properties.opacity,
            type: LayoutAnimation.Types.linear,
            springDamping: 0.4,
        }
    },
    login: {
        duration: 200,
        create: {
            property: LayoutAnimation.Properties.scaleXY,
            type: LayoutAnimation.Types.easeInEaseOut,
            springDamping: 1,
        },
        update: {
            property: LayoutAnimation.Properties.opacity,
            type: LayoutAnimation.Types.spring,
            springDamping: 0.4,
        },
        delete: {
            duration: 200,
            property: LayoutAnimation.Properties.opacity,
            type: LayoutAnimation.Types.linear,
            springDamping: 0.4,
        }
    },
};

export class AnimsUtils {
    static configureNext(anim? = Anims.main) {
        LayoutAnimation.configureNext(anim);
    }
}

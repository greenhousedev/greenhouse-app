import {ScaledSheet} from 'react-native-size-matters';
import {AppTheme} from './AppTheme';

export const CommonStyles = ScaledSheet.create({
  fillParent: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  screen: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
  },

  screenPadding: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
    padding: AppTheme.screen.padding,
  },

  listScreen: {
    flex: 1,
    backgroundColor: AppTheme.backgroundColor,
    paddingLeft: AppTheme.screen.padding,
    paddingRight: AppTheme.screen.padding,
  },

  list: {
    flex: 1,
    paddingLeft: AppTheme.screen.padding,
    paddingRight: AppTheme.screen.padding,
  },

  listItem: {
    marginLeft: AppTheme.screen.padding,
    marginRight: AppTheme.screen.padding,
  },

  rightNavButton: {
    marginRight: '16@s',
  },
});

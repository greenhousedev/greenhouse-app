import {Colors} from './Colors';
import {scale} from 'react-native-size-matters';
import {Dimen} from './Dimen';
import FuturaText, {
  FontSize,
  FontStyle,
  styleFontSize,
  styleFontStyle,
} from '../view/component/FuturaText';
import {Platform} from 'react-native';

export const AppTheme = {
  primaryColor: Colors.teal,
  backgroundColor: Colors.white,
  alertColor: Colors.reddish,

  headerTitleStyle: {
    ...styleFontStyle(FontStyle.MEDIUM),
    ...styleFontSize(FontSize.MEDIUM),
    color: Colors.darkGrey,
  },

  bottomBar: {
    activeColor: Colors.teal,
    inactiveColor: Colors.grey,
    iconSize: scale(24),
    height: Platform.OS === 'ios' ? scale(100) : scale(70),

    tab: {
      marginTop: scale(10),
      marginBottom: scale(10),
    },
  },

  input: {
    iconSize: scale(16),
    height: Dimen.elementHeight,
    borderRadius: Dimen.elementRadius,
  },

  button: {
    height: Dimen.elementHeight,
    borderRadius: Dimen.elementRadius,
  },

  screen: {
    padding: scale(16),
  },
};

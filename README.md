# Greenhouse App

This repository contains 2 apps, one Android and one iOS mobile app
that can help controll a greenhouse.

The controll fabric is handled by firebase and the actual execution
of the commands is handled by the following open source code:
[Greenhouse Raspberry](https://gitlab.com/greenhousedev/greenhouseraspberry)




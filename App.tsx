/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import 'react-native-gesture-handler';
import {enableScreens} from 'react-native-screens';
import RootContainer from './src/navigation/RootContainer';
import AppNavigator from './src/navigation/AppNavigator';
import {Linking} from 'react-native';
import dynamicLinks, {
  FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';
import UserManager from './src/controller/UserManager';
import LoginManager from './src/controller/LoginManager';

enableScreens();

declare const global: {HermesInternal: null | {}};

const useMount = (func) => useEffect(() => func(), []);

export const useInitialURL = () => {
  const [url, setUrl] = useState<string | null>();
  const [processing, setProcessing] = useState(true);

  useMount(() => {
    const getUrlAsync = async () => {
      // Get the deep link used to open the app
      const initialUrl = await Linking.getInitialURL();

      // The setTimeout is just for testing purpose
      setTimeout(() => {
        setUrl(initialUrl);
        setProcessing(false);
      }, 1000);
    };

    getUrlAsync();
  });

  return {url, processing};
};

const App = () => {
  //  firebase dynamic linking listener
  useEffect(() => {
    dynamicLinks()
      .getInitialLink()
      .then((link) => {
        console.log('App::getInitialLink');
        handleDynamicLink(link);
      });
    const linkingListener = dynamicLinks().onLink(handleDynamicLink);
    return () => {
      linkingListener();
    };
  }, []);

  const handleDynamicLink = (
    link: FirebaseDynamicLinksTypes.DynamicLink | null,
  ) => {
    console.log('App::handleDynamicLink link=' + JSON.stringify(link));
    if (link && link.url && LoginManager.isLoggedIn) {
      if (link.url.includes('confirmEmail')) {
        const userKey = 'userId%3D';
        const endPart = '&lang=';
        if (link.url.includes(userKey)) {
          let userId = link.url.substring(
            link.url.indexOf(userKey) + userKey.length,
            link.url.includes(endPart) ? link.url.indexOf(endPart) : undefined,
          );
          console.log('App::handleDynamicLink confirmEmail userId=' + userId);
          if (userId && userId === LoginManager.userId) {
            UserManager.confirmLink = link.url;
            AppNavigator.goToLoggedIn();
          }
        }
      } else if (link.url.includes('confirmedEmail')) {
        AppNavigator.goToLoggedIn();
      }

      //UserManager.confirmLink = link.url;
      //AppNavigator.goToLoggedIn();
    }
  };

  return (
    <RootContainer
      navigationRef={AppNavigator.setTopLevelNavigator}
      ref={AppNavigator.setRootContainerRef}
    />
  );
};

export default App;
